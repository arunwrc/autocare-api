ALTER TABLE `repair_order_services` CHANGE `modified_at` `updated_at` TIMESTAMP NOT NULL;
ALTER TABLE `repair_orders` CHANGE `modified_at` `updated_at` TIMESTAMP NOT NULL;
ALTER TABLE `repair_order_spares` CHANGE `modified_at` `updated_at` TIMESTAMP NOT NULL;
ALTER TABLE `repair_order_operations` CHANGE `modified_at` `updated_at` TIMESTAMP NOT NULL;

ALTER TABLE `repair_order_spares_issue` CHANGE `is` `id` INT(11) NOT NULL AUTO_INCREMENT;


#04/08/2017
ALTER TABLE `users`  ADD `auth_token_expire` DATETIME NULL DEFAULT NULL  AFTER `auth_token`;

#05/08/2017
--
-- Table structure for table `access_levels`
--

CREATE TABLE IF NOT EXISTS `access_levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `user_types` ADD `access_levels` TEXT NOT NULL ;

-----------------       2017-08-24      ---------------
ALTER TABLE `operation_spares` CHANGE `modified_at` `updated_at` TIMESTAMP NOT NULL;

ALTER TABLE `service_operations` CHANGE `modified_at` `updated_at` TIMESTAMP NOT NULL;