<?php
//RO STATUSES
define("RO_STATUS_STARTED",1);
define("RO_STATUS_SPARE_ISSUED",2);
define("RO_STATUS_OPERATION_COMPLETED",3);
define("RO_STATUS_QA_COMPLETED",4);
define("RO_STATUS_NEXT_SERVICE_ADDED",5);
define("RO_STATUS_PREBILL_GENERATED",6);
define("RO_STATUS_BILL_GENERATED",7);
define("RO_STATUS_BILL_PAID",8);
define("RO_STATUS_COMPLETED",9);

//bill status
define("BILL_STATUS_PENDING",1);
define("BILL_STATUS_PAID",2);
define("BILL_STATUS_CANCELLED",3);

//booking types
define("BOOKING",1);
define("NEXT_SERVICE", 2);

define("BOOKING_NUMBER_PREFIX", "BK");
define("RO_NUMBER_PREFIX", "RO");
define("BILL_NUMBER_PREFIX", "INV");


$ro_next_status_link = array(
	RO_STATUS_STARTED => array('label'=>"Issue Spare",'link'=>"issue-spares","icon"=>"fa-wrench"),
	RO_STATUS_SPARE_ISSUED => array('label'=>"Operation Complete",'action'=>"ro-operation-complete","icon"=>"fa-automobile"),
	RO_STATUS_OPERATION_COMPLETED => array('label'=>"QA Complete",'action'=>"ro-qa-complete","icon"=>"fa-thumbs-up"),
	RO_STATUS_QA_COMPLETED => array('label'=>"Add Next Service",'action'=>"ro-next-service-add","icon"=>"fa-plus"),
	RO_STATUS_NEXT_SERVICE_ADDED => array('label'=>"Pre Bill",'action'=>"ro-pre-bill","icon"=>"fa-file-pdf-o"),
	RO_STATUS_PREBILL_GENERATED =>  array('label'=>"Bill",'link'=>"create-bill","icon"=>"fa-file-pdf-o"),
	RO_STATUS_BILL_GENERATED => array('label'=>"Payment",'action'=>"ro-paid","icon"=>"fa-paypal"),
	RO_STATUS_BILL_PAID => array('label'=>"Complete",'action'=>"ro-complete","icon"=>"fa-check")
						);
?>