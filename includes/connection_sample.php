<?php
class dbConnect{

	private $connection;

	public function __construct()
	{
		$this->connection = mysqli_connect("db_host", "db_user", "db_password", "db_name");

		/* check connection */
		if (mysqli_connect_errno()) {
    		printf("Connect failed: %s\n", mysqli_connect_error());
    		exit();
		}

	}

	public function _query($sql)
	{
		if ($result = mysqli_query($this->connection, $sql)) {
    		return $result;
		}
	}

	public function rows($sql)
	{
		$result = $this->_query($sql);
		$rows=array();
		while($row = mysqli_fetch_array($result,MYSQLI_NUM)){
			$rows[] = $row;
		}
		// Free result set
		mysqli_free_result($result);
		$this->_close();
		return $rows;
	}

	public function rows_assoc($sql)
	{
		$result = $this->_query($sql);
		$rows=array();
		if ($result->num_rows > 0) {
			while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
				$rows[] = $row;
			}
		}else{
			$rows = false;
		}
		// Free result set
		mysqli_free_result($result);
		//$this->_close();
		return $rows;
	}

	public function row($sql)
	{
		$result = $this->_query($sql);
		$row = mysqli_fetch_array($result,MYSQLI_NUM);
		// Free result set
		mysqli_free_result($result);
		$this->_close();
		return $row;
	}

	public function row_assoc($sql)
	{
		$result = $this->_query($sql);
		$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
		// Free result set
		mysqli_free_result($result);
		//$this->_close();
		return $row;
	}

	public function get_count($sql)
	{
		$result = $this->_query($sql);
		$count = $result->num_rows;
		mysqli_free_result($result);
		//$this->_close();
		return $count;
	}
	public function delete_data($table,$where)
	{
		$sql = "DELETE FROM ".$table." WHERE ".$where;
		
		$this->_query($sql);
	}

	public function insert_data($table,$data)
	{
		$field_str = $value_str = '';
		foreach($data as $field=>$value)
		{
			$field_str .= $field.",";
			$value_str .= "'".mysqli_real_escape_string($this->connection,$value)."',";
		}

		// Remove the comma at the end of the string
		$field_str = rtrim($field_str,",");
		$value_str = rtrim($value_str,",");

		$sql = "INSERT INTO ".$table."(".$field_str.") VALUES(".$value_str.")";
		//echo $sql;exit;
		$this->_query($sql);
		return $this->_insert_id();

	}

	public function _insert_id()
	{
		return mysqli_insert_id($this->connection);
	}

	public function update_data($table,$data,$filter=null)
	{
		$value_str = '';
		foreach($data as $field=>$value)
		{
			$value_str .= $field."='".mysqli_real_escape_string($this->connection,$value)."',";
		}
		// Remove the comma at the end of the string
		$value_str = rtrim($value_str,",");

		$sql = "UPDATE ".$table." SET ".$value_str;
		if($filter != null){
			$sql .=" WHERE ".$filter;
		}
		//echo $sql;exit;
		$this->_query($sql);
		return $this->_affected_rows();

	}


	public function _affected_rows()
	{
		return mysqli_affected_rows($this->connection);
	}


	public function _close()
	{
		mysqli_close($this->connection);

	}
}
?>