<?php
require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');

function ro_pdf($data)
{
	$pdf =  new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Autocare');
    $pdf->SetTitle('Repair Order');

    // set default header data
    $pdf->SetHeaderData('', '','Repair Order','');

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    $pdf->AddPage();

    $pdf->SetFont('helvetica', '', 10);


    ob_start();

    $html ='
        <table  border="1" cellspacing="5">
            <tr>
                <td><h3>Service Dealer Details</h3></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="5">
                        <tr><td><b>Dealer Code</b></td><td width="5%">:</td><td>KL03001</td></tr>
                        <tr><td><b>Dealer Name</b>  </td><td>:</td><td>Muthoot Motors</td></tr>
                        <tr><td colspan="3">37/177A,Near KSEB Office, Ernakulam Aluva Road Palarivattom Cochin Ernakulam Kerala India 682025</td></tr>
                        <tr><td><b>Tel:</b> 4804087408</td><td></td><td><b>FAX NO:</b> </td></tr>
                        <tr><td colspan="3"><b>Email:</b> sales@muthoothonda.com</td></tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr><td><b>Order No :</b> '.$data['ro_number'].'</td></tr>
                        <tr><td><b>Date :</b> '.$data['ro_date'].'</td></tr>
                    </table>
                </td>
            </tr>

                
            </tr>
        </table>';
    
   
    $html .='
        <table  border="1" cellspacing="5">
            <tr>
                <td><h3>Customer Details</h3></td>
                <td><h3>Vehicle Details</h3></td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="5">
                        <tr><td>Name</td><td width="5%">:</td><td>'.$data['Customer'].'</td></tr>
                        <tr><td>Address</td><td>:</td><td>'.$data['CustomerAddress'].'</td></tr>
                        <tr><td>Contact No</td><td>:</td><td>'.$data['CustomerMobile'].'</td></tr>
                        <tr><td>Email</td><td>:</td><td>'.$data['CustomerEmail'].'</td></tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr><td> Reg No</td><td width="5%">:</td><td>'.$data['reg_no'].'</td></tr>
                        <tr><td> Make</td><td width="5%">:</td><td>'.$data['VehicleMake'].'</td></tr>
                        <tr><td> Model</td><td width="5%">:</td><td>'.$data['VehicleModel'].'</td></tr>
                        <tr><td> Engine No</td><td width="5%">:</td><td>'.$data['engine_no'].'</td></tr>
                        <tr><td> Chasis No</td><td width="5%">:</td><td>'.$data['chasis_no'].'</td></tr>
                        <tr><td> Color</td><td width="5%">:</td><td>'.$data['color'].'</td></tr>
                    </table>
                </td>
            </tr>

                
            </tr>
        </table>';

        $services = '';
        foreach($data['service'] as $row_service)
        {
            $services .= '<tr><td>Service Type</td><td width="5%">:</td><td>'.$row_service.'</td></tr>';
        }

        $html .='<table  border="0" cellspacing="5">
            <tr>
                <td colspan="3"><h3>Service Details</h3></td>
            </tr>
            '.$services.'     
        </table>
        ';

        $html .='<br>';

        $operations = '';
        foreach($data['operations'] as $opr)
        {
            $operations .= '<tr><td>'.$opr['OperationName'].'</td><td>'.$opr['rate'].'</td><td>'.$opr['quantity'].'</td></tr>';
        }
        $spares = '';
        foreach($data['spares'] as $spr)
        {
            $spares .= '<tr><td>'.$spr['SpareName'].'</td><td>'.$spr['SpareRate'].'</td><td>'.$spr['quantity'].'</td></tr>';
        }


        $html .='
        <table  border="1" cellspacing="5">
            <tr>
                <td><h3>Operations</h3></td>
                <td><h3>Spares</h3></td>
            </tr>
            <tr>
                <td>
                    <table border="1" cellpadding="3">
                        <tr><td><b>Name</b></td><td><b>Rate</b></td><td><b>Quantity</b></td></tr>
                        '.$operations.'
                    </table>
                </td>
                <td>
                    <table border="1" cellpadding="3">
                        <tr><td><b>Name</b></td><td><b>Rate</b></td><td><b>Quantity</b></td></tr>
                        '.$spares.'
                    </table>
                </td>
            </tr>

                
            </tr>
        </table>';


    ob_end_clean();

	$pdf->writeHTML($html, true, false, false, false, '');
	$file_name='ro.pdf';

	return $pdf->Output('My cool PDF', 'S');
}

//bill

function bill_pdf($data)
{
    $pdf =  new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Autocare');
    $pdf->SetTitle('Invoice');

    // set default header data
    $pdf->SetHeaderData('', '','Invoice','');

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    $pdf->AddPage();

    $pdf->SetFont('helvetica', '', 10);


    ob_start();

    $html ='
        <table  border="1" cellspacing="5">
            <tr>
                <td><h3>Service Dealer Details</h3></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="5">
                        <tr><td><b>Dealer Code</b></td><td width="5%">:</td><td>KL03001</td></tr>
                        <tr><td><b>Dealer Name</b>  </td><td>:</td><td>Muthoot Motors</td></tr>
                        <tr><td colspan="3">37/177A,Near KSEB Office, Ernakulam Aluva Road Palarivattom Cochin Ernakulam Kerala India 682025</td></tr>
                        <tr><td><b>Tel:</b> 4804087408</td><td></td><td><b>FAX NO:</b> </td></tr>
                        <tr><td colspan="3"><b>Email:</b> sales@muthoothonda.com</td></tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr><td><b>Invoice No :</b> '.$data['bill_number'].'</td></tr>
                        <tr><td><b>Date :</b> '.$data['bill_date'].'</td></tr>
                    </table>
                </td>
            </tr>

                
            </tr>
        </table>';
    
   
    $html .='
        <table  border="1" cellspacing="5">
            <tr>
                <td><h3>Customer Details</h3></td>
                <td><h3>Vehicle Details</h3></td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="5">
                        <tr><td>Name</td><td width="5%">:</td><td>'.$data['Customer'].'</td></tr>
                        <tr><td>Address</td><td>:</td><td>'.$data['CustomerAddress'].'</td></tr>
                        <tr><td>Contact No</td><td>:</td><td>'.$data['CustomerMobile'].'</td></tr>
                        <tr><td>Email</td><td>:</td><td>'.$data['CustomerEmail'].'</td></tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr><td> Reg No</td><td width="5%">:</td><td>'.$data['reg_no'].'</td></tr>
                        <tr><td> Make</td><td width="5%">:</td><td>'.$data['VehicleMake'].'</td></tr>
                        <tr><td> Model</td><td width="5%">:</td><td>'.$data['VehicleModel'].'</td></tr>
                        <tr><td> Engine No</td><td width="5%">:</td><td>'.$data['engine_no'].'</td></tr>
                        <tr><td> Chasis No</td><td width="5%">:</td><td>'.$data['chasis_no'].'</td></tr>
                        <tr><td> Color</td><td width="5%">:</td><td>'.$data['color'].'</td></tr>
                    </table>
                </td>
            </tr>

                
            </tr>
        </table>';

        

       // $html .='<br>';

        $operations = '';
        $slno = 1;
        $total = 0;
        foreach($data['operations'] as $opr)
        {
            $line_total = $opr['rate']*$opr['quantity'];
            $total += $line_total;
            $operations .= '<tr><td>'.$slno.'</td><td>Operation - '.$opr['OperationName'].'</td><td>'.$opr['quantity'].'</td><td>'.number_format($opr['rate'],2).'</td><td>'.number_format($line_total,2).'</td></tr>';
            $slno++;
        }
        $spares = '';
        foreach($data['spares'] as $spr)
        {
            $line_total = $spr['SpareRate']*$spr['quantity'];
            $total += $line_total;
            $spares .= '<tr><td>'.$slno.'</td><td>Spare - '.$spr['SpareName'].'</td><td>'.$spr['quantity'].'</td><td>'.number_format($spr['SpareRate'],2).'</td><td>'.number_format($line_total,2).'</td></tr>';
            $slno++;
        }

       

        $html .='
        <table  border="1" cellspacing="5">
            <tr>
                <td><h3>Service Details</h3></td>
            </tr>
            <tr>
                <td>
                    <table border="1" cellpadding="5">
                    <tr>
                        <td><h4>Slno</h4></td>
                        <td><h4>Operation/Spare</h4></td>
                        <td><h4>Quantity</h4></td>
                        <td><h4>Price</h4></td>
                        <td><h4>Total</h4></td>
                    </tr>
                    '.$operations.'
                    '.$spares.'

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><h4>Total</h4></td>
                        <td>'.number_format($total,2).'</td>
                    </tr>

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><h4>Discount</h4></td>
                        <td>'.number_format($data['discount'],2).'</td>
                    </tr>

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><h4>Tax</h4></td>
                        <td>'.number_format($data['tax'],2).'</td>
                    </tr>

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><h4>Grand Total</h4></td>
                        <td>'.number_format($total-$data['discount']+$data['tax'],2).'</td>
                    </tr>

                    </table>
                </td>
            </tr>


                
            </tr>
        </table>';



    ob_end_clean();

    $pdf->writeHTML($html, true, false, false, false, '');
    $file_name='invoice.pdf';

    return $pdf->Output('My cool PDF', 'S');
}

?>