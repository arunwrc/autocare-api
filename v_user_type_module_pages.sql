CREATE OR REPLACE ALGORITHM=UNDEFINED DEFINER=`adminKgzBkAy`@`127.8.11.131` SQL SECURITY DEFINER VIEW `v_user_type_modules` AS
SELECT page.id AS PageId,page.name AS PageName,page.module_id AS ModuleId,module.name AS ModuleName,
	page.page_url AS PageUrl,0 AS UserTypeId,'No' AS PageAccess 
FROM module_pages AS page 
JOIN modules AS module ON module.id = page.module_id 

UNION

SELECT page.id AS PageId,page.name AS PageName,page.module_id AS ModuleId,module.name AS ModuleName,
	page.page_url AS PageUrl,usr_type.id AS UserTypeId,if(isnull(usr_page.id),'No','Yes') AS PageAccess,usr_page.id
FROM module_pages AS page 
JOIN user_types AS usr_type 
JOIN modules AS module ON module.id = page.module_id 
LEFT JOIN user_type_module_pages AS usr_page ON usr_page.module_page_id = page.id


