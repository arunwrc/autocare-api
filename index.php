<?php
session_start();
require 'vendor/autoload.php';
require 'includes/connection.php';
require 'includes/constants.php';

require('includes/my_pdf.php');

require_once('util.php');

define("SYSTEM_ACCESS_YES",1);
define("SYSTEM_ACCESS_NO",0);



$container = new \Slim\Container;
$app = new Slim\App($container);


login();


//urls
$app->get('/', 'check_access');

$app->get('/module-pages', function($request, $response, $args) {
    
    $dbConnect = new dbConnect();
    $strSQL = "SELECT pages.id as PageId,pages.name as PageName,pages.module_id as ModuleId,
                modules.name as ModuleName,pages.page_url as PageUrl
                FROM module_pages as pages
                JOIN modules ON modules.id = pages.module_id
                ORDER BY modules.id";
   // echo $strSQL;exit();
    $data = $dbConnect->rows_assoc($strSQL);
    if($data){
        echo json_encode(array("data" => $data,"status" => true));
    }else{
        echo json_encode(array("data" => null,"status" => false));
    }
});


$app->get('/user-type-pages/{user_type}', function($request, $response, $args) {
    
    $user_type_id = $args['user_type'];
    //echo $user_type_id;exit();
    $dbConnect = new dbConnect();
    $strSQL = "SELECT module_page_id as ModulePageId
                FROM user_type_module_pages 
                WHERE user_type_id = '".$user_type_id."'";
   // echo $strSQL;exit();
    $data = $dbConnect->rows_assoc($strSQL);
    if($data){
        echo json_encode(array("data" => $data,"status" => true));
    }else{
        echo json_encode(array("data" => null,"status" => false));
    }
});

$app->post('/user-privilages',function(){

    // echo "<pre>";print_r($_POST);exit();
    $dbConnect = new dbConnect();
    $user_type_id   = $_POST['user_type'];
    $module_pages     = $_POST['page_check'];
    foreach($module_pages as $page_id)
    {
        $data = array('module_page_id' => $page_id,
                        'user_type_id'=> $user_type_id);
       
        $strSQL = "SELECT id
                FROM user_type_module_pages 
                WHERE user_type_id = '".$user_type_id." AND module_page_id = '".$page_id."'";
        $exists = $dbConnect->row_assoc($strSQL);
        if(!$exists){
            $insert []= $dbConnect->insert_data('user_type_module_pages',$data);
        }
        
    }
  
    echo json_encode(array("data" => null,"status" => true,'message'=>"User Privilages Updated Successfully!"));

});

$app->get('/dashboard/{user_type}',function($request, $response, $args){
    $user_type_id = $args['user_type'];
    $function = 'dashboardContent'.$user_type_id;
    if(function_exists($function)){
        $content = $function();
        $response =  json_encode(array("status"=>true,"data"=>$content));
    }else{
        $response = json_encode(array("status"=>false,"data"=>null));
    }
   
    echo $response;

});


$app->get('/logout', function($request, $response, $args) {
    
    $dbConnect = new dbConnect();
    $dbConnect->delete_data('autocare_sessions',"ip_address = '".$_SERVER['REMOTE_ADDR']."'");
    $token = $request->getHeaderLine('access_token');

    $strSQL = "SELECT id FROM users WHERE auth_token = '".$token."'";
    //echo $strSQL;exit();
    $data = $dbConnect->row_assoc($strSQL);

    $updateUser = array('auth_token_expire'=>date2sql());
    $update = $dbConnect->update_data('users',$updateUser,"id = '".$data['id']."'");
    echo json_encode(array("data" => null,"status" => true,'message'=>"Your are successfully Logout"));
});
$app->post('/user/change-password','changePassword');


//Routes
$app->get('/welcome', function($request, $response, $args) {
    echo "Welcome Autocare-api";
});

$app->get('/genders', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    $data = $dbConnect->rows_assoc("SELECT * FROM genders ORDER BY id");
    echo json_encode($data);
});

$app->get('/usertypes', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    $data = $dbConnect->rows_assoc("SELECT * FROM user_types ORDER BY id");
    echo json_encode($data);
});

$app->get('/designations', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    $data = $dbConnect->rows_assoc("SELECT * FROM designations ORDER BY id");
    echo json_encode($data);
});

$app->get('/employee-options', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    $data = $dbConnect->rows_assoc("SELECT id,CONCAT(first_name,' ',last_name) as EmpName FROM employees ORDER BY id");
    echo json_encode($data);
});

$app->get('/customer-options', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    $data = $dbConnect->rows_assoc("SELECT id,CONCAT(first_name,' ',last_name) as CustomerName FROM customers ORDER BY id");
    echo json_encode($data);
});

$app->get('/booking-options', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    $data = $dbConnect->rows_assoc("SELECT booking.*,CONCAT(customers.first_name,' ',customers.last_name) as CustomerName FROM booking,customers WHERE booking.customer_id = customers.id ORDER BY id");
    echo json_encode($data);
});

$app->get('/feedbacks', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    $data = $dbConnect->rows_assoc("SELECT * FROM feedbacks ORDER BY id");
    echo json_encode($data);
});


$app->get('/employeestatuses', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    $data = $dbConnect->rows_assoc("SELECT * FROM employee_status ORDER BY id");
    echo json_encode($data);
});

$app->get('/vehicle-models', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    $data = $dbConnect->rows_assoc("SELECT * FROM vehicle_models ORDER BY id");
    echo json_encode($data);
});

$app->get('/vehicle-makes', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    $data = $dbConnect->rows_assoc("SELECT * FROM vehicle_makes ORDER BY id");
    echo json_encode($data);
});

$app->get('/vehicle-types', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    $data = $dbConnect->rows_assoc("SELECT * FROM vehicle_types ORDER BY id");
    echo json_encode($data);
});

$app->get('/booking-types', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    $data = $dbConnect->rows_assoc("SELECT * FROM booking_types ORDER BY id");
    echo json_encode($data);
});

$app->get('/vehicles', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    $data = $dbConnect->rows_assoc("SELECT * FROM vehicles ORDER BY id");
    echo json_encode($data);
});

$app->get('/customer-vehicles/{customer}', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    $data = $dbConnect->rows_assoc("SELECT * FROM vehicles WHERE customer_id = '".$args['customer']."' ORDER BY id");
    echo json_encode($data);
});

$app->get('/services', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    $data = $dbConnect->rows_assoc("SELECT id,name FROM services ORDER BY name");
    echo json_encode($data);
});
$app->get('/operations', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    $data = $dbConnect->rows_assoc("SELECT id,name FROM operations ORDER BY name");
    echo json_encode($data);
});

$app->get('/spares', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    $data = $dbConnect->rows_assoc("SELECT name as ItemName,id as ItemId FROM spares ORDER BY id");
    echo json_encode($data);
});

$app->get('/autofill/customers/{str}', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    
    $like = $args['str'];
    $sql = "SELECT id,CONCAT(first_name,' ',last_name, ' (',phone,')') as name FROM customers";
    $sql .= " WHERE first_name LIKE '%".$like."%' || last_name LIKE '%".$like."%'";
    $sql .= " LIMIT 5";
    $data = $dbConnect->rows_assoc($sql);
    if($data){
        $response = array("status"=>true,"data"=>$data);
    }else{
        $response = array("status"=>false,"data"=>null);
    }
    echo json_encode($response);
});

$app->get('/autofill/vehicles/{str}', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    
    $like = $args['str'];
    $sql = "SELECT veh.id,CONCAT(make.name,' ',model.name,' ',veh.reg_no) as name 
            FROM vehicles veh
            LEFT JOIN vehicle_makes make ON make.id= veh.vehicle_make_id
            LEFT JOIN vehicle_models model ON model.id= veh.vehicle_model_id";
    $sql .= " WHERE reg_no LIKE '%".$like."%'";

    $data = $dbConnect->rows_assoc($sql);
    if($data){
        $response = array("status"=>true,"data"=>$data);
    }else{
        $response = array("status"=>false,"data"=>null);
    }
    echo json_encode($response);
});

//get user data
$app->get('/vehicle/{id}', function ($request, $response, $args) {
    $dbConnect = new dbConnect();
    $strSQL = "SELECT * 
                FROM vehicles
                WHERE id = '".$args['id']."'";
    $data = $dbConnect->row_assoc($strSQL);

    if($data){
        echo json_encode(array("status" => true,"data"=>$data,"message"=>"Vehicle rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No Vehicle data"));
    }
});


$app->get('/service-operations/{service_id}', function($request, $response, $args) {
    $service_id = $args['service_id'];
    $dbConnect = new dbConnect();
    $sql = "SELECT SO.*,O.rate as OperationRate,O.name as OperationName,S.rate as ServiceRate 
            FROM service_operations SO
            LEFT JOIN services S ON S.id = SO.service_id
            LEFT JOIN operations O ON O.id=SO.operation_id
            WHERE SO.service_id = '".$service_id."'
            ORDER BY O.name";
    $data = $dbConnect->rows_assoc($sql);
    if($data)
        echo json_encode(array("status" => true,"data"=>$data,"message"=>"Service Operations rendered successfully"));
    else
         echo json_encode(array("status" => false,"data"=>null,"message"=>"No Service Operations"));
});

$app->get('/operation-spares/{operation_id}', function($request, $response, $args) {
    $operation_id = $args['operation_id'];
    $dbConnect = new dbConnect();
    $sql = "SELECT OS.*,O.rate as OperationRate,S.rate as SpareRate,S.name as SpareName,S.units as SpareUnit 
            FROM operation_spares OS
            LEFT JOIN operations O ON O.id=OS.operation_id
            LEFT JOIN spares S ON S.id = OS.spare_id
            WHERE OS.operation_id = '".$operation_id."'
            ORDER BY O.name";
    $data = $dbConnect->rows_assoc($sql);

    if($data)
        echo json_encode(array("status" => true,"data"=>$data,"message"=>"Operation Spares rendered successfully"));
    else
         echo json_encode(array("status" => false,"data"=>null,"message"=>"No Operation Spares"));
});
//-------------------------------------------------------------------



$app->get('/user/session', function($request, $response, $args) {
    $dbConnect = new dbConnect();
    print_r($_SESSION);exit();
    if(isset($_SESSION['loggedinUser'])){
        echo json_encode(array("data" => $_SESSION['loggedinUser'],"status" => true));
    }else {
        echo "fail";
    }
});

$app->post('user/unique',function(){
    $_POST = json_decode(file_get_contents("php://input"), true);    
    $username = $_POST['username'];

    $strSQL = "SELECT * FROM users WHERE username='" . $username . "'" ;
    $dbConnect = new dbConnect();
    $count = $dbConnect->get_count($strSQL);
    if($count==0){
        echo json_encode(array("status" => true));
    }else {
        echo json_encode(array("status" => false));
    }
});

$app->post('/user/create','addUser');
$app->post('/user/edit','editUser');

//get user data
$app->get('/user/{id}', function ($request, $response, $args) {
    $dbConnect = new dbConnect();
    $strSQL = "SELECT employees.* ,users.user_type_id,users.username
                FROM employees 
                LEFT JOIN users ON users.employee_id = employees.id
                WHERE employees.id = '".$args['id']."'";
    $data = $dbConnect->row_assoc($strSQL);

    if($data){
        echo json_encode(array("status" => true,"data"=>$data,"message"=>"User rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No User data"));
    }
});

//get users list
$app->get('/users',function(){
    $page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
    $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
    $search = isset($_GET['search']) ? $_GET['search'] : '';

    $offset = (--$page) * $limit;

    $dbConnect = new dbConnect();
    $strSQL = "SELECT CONCAT(employees.first_name,' ',employees.last_name) AS EmployeeName,
                designations.name as Designation,
                employees.dob ,
                users.phone,
                users.username,
                users.email,
                employee_status.name AS EmployeeStatus,
                users.active,users.id
                FROM users 
                LEFT JOIN employees ON employees.id= users.employee_id
                LEFT JOIN employee_status ON employee_status.id= employees.employee_status_id
                LEFT JOIN designations ON designations.id= employees.employee_current_designation_id
                WHERE users.active!=0";
    if(trim($search) != ''){
        $strSQL .=" AND (employees.first_name LIKE '%".$search."%'";
        $strSQL .=" OR employees.last_name LIKE '%".$search."%'";
        $strSQL .=" OR users.phone LIKE '%".$search."%'";
        $strSQL .=" OR designations.name LIKE '%".$search."%'";
        $strSQL .=" OR users.email LIKE '%".$search."%'";
         $strSQL .=" OR users.username LIKE '%".$search."%'";
        $strSQL .=" )";
    }

    $count = $dbConnect->get_count($strSQL);
    $strSQL .= " ORDER BY users.id DESC";
    $strSQL_limit = $strSQL." LIMIT ".$limit." OFFSET ".$offset;
   
    $data = $dbConnect->rows_assoc($strSQL_limit);

    if($data){
        echo json_encode(array("status" => true,"count"=>$count,"data"=>$data,"message"=>"Users rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No Users data"));
    }
});

$app->get('/customers',function(){

    $page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
    $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
    $search = isset($_GET['search']) ? $_GET['search'] : '';
    $customer_id = isset($_GET['key']) ? $_GET['key'] : '';

    $offset = (--$page) * $limit;

    $dbConnect = new dbConnect();
    $strSQL = "SELECT C.*,G.name as Gender FROM customers C,genders G
                WHERE C.gender_id = G.id";
    if(trim($customer_id) != ''){
        $strSQL .=" AND C.id = ".$customer_id;
    }else if(trim($search) != ''){
        $strSQL .=" AND (C.first_name LIKE '%".$search."%'";
        $strSQL .=" OR C.last_name LIKE '%".$search."%'";
        $strSQL .=" OR C.mobile LIKE '%".$search."%'";
        $strSQL .=" OR C.phone LIKE '%".$search."%'";
        $strSQL .=" OR C.email LIKE '%".$search."%'";
        $strSQL .=" )";
    }
    $count = $dbConnect->get_count($strSQL);
    $strSQL .= " ORDER BY C.id DESC";
    $strSQL_limit = $strSQL." LIMIT ".$limit." OFFSET ".$offset;

   
    $data = $dbConnect->rows_assoc($strSQL_limit);
    if($data){
        echo json_encode(array("status" => true,"count"=>$count,"data"=>$data,"message"=>"Customers rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"count"=>$count,"data"=>null,"message"=>"No Customers data"));
    }
});

$app->post('/customer','manageCustomer');

//get user data
$app->get('/customer/{id}', function ($request, $response, $args) {
    $dbConnect = new dbConnect();
    $strSQL = "SELECT customers.*
                FROM customers 
                WHERE customers.id = '".$args['id']."'";
    $data = $dbConnect->row_assoc($strSQL);

    if($data){
        echo json_encode(array("status" => true,"data"=>$data,"message"=>"Customer rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No Customer data"));
    }
});

//------------------------------------------------------------------------------------------
$app->post('/manage-spare',function($request,$response,$args){
    $dbConnect = new dbConnect();

    $spare = array(
        'name' => $_POST['name'],
        'description' => $_POST['description'],
        'rate' => $_POST['rate'],
        'units' => $_POST['units'],
        'image' => @$image,
        'attached_file' => @$attached_file,
        'active' => 1,
        'system_user_id' => @$_SESSION['logged_in_user'],
        'updated_at' => current_datetime(),
        );  

    if($_POST['spare_id'] != "" && $_POST['spare_id'] > 0){
        $filter = "id = '".$_POST['spare_id']."'";
        $update = $dbConnect->update_data('spares',$spare,$filter);
        if($update)
        { 
            $response = array("status"=>true,"message"=>"Spare updated successfully.","data"=>null);
        
        }else{
            $response = array("status"=>false,"message"=>"Failed to update spare.","data"=>null);
        }
    }else{
        $spare['created_at'] = current_datetime();
        $spare_id = $dbConnect->insert_data('spares',$spare);
        if($spare_id)
        { 
            $response = array("status"=>true,"message"=>"New Spare added successfully.","data"=>null);
        
        }else{
            $response = array("status"=>false,"message"=>"Failed to add spare.","data"=>null);
        }
    }
   // echo "<pre>";print_r($spare);exit();
    
    

    echo json_encode($response);
});

$app->get('/list-spare',function(){
    global $ro_next_status_link;
    $page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
    $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
    $search = isset($_GET['search']) ? $_GET['search'] : '';
    $customer_id = isset($_GET['key']) ? $_GET['key'] : '';

    $offset = (--$page) * $limit;

    $dbConnect = new dbConnect();
    $strSQL = "SELECT spare.id,spare.code,spare.name,spare.description,FORMAT(spare.rate,2) as rate,spare.units
             FROM spares spare";
    $strSQL .=" WHERE 1";
    if(trim($search) != ''){
        $strSQL .=" AND spare.name LIKE '%".$search."%'";
    }
    $count = $dbConnect->get_count($strSQL);
    $strSQL .= " ORDER BY spare.id DESC";
    $strSQL_limit = $strSQL." LIMIT ".$limit." OFFSET ".$offset;
   
    $data = $dbConnect->rows_assoc($strSQL_limit);

    if($data){
        
        echo json_encode(array("status" => true,"count"=>$count,"data"=>$data,"message"=>"Spares rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No Spares"));
    }
});

$app->get('/spare/{id}', function ($request, $response, $args) {
    $dbConnect = new dbConnect();
    $strSQL = "SELECT *
                FROM spares
                WHERE id = '".$args['id']."'";
    $data = $dbConnect->row_assoc($strSQL);

    if($data){
        echo json_encode(array("status" => true,"data"=>$data,"message"=>"Spare rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No Spare data"));
    }
});
//---------------------------------------------------------------------------

$app->post('/manage-operation',function($request,$response,$args){
    $dbConnect = new dbConnect();
    //echo "<pre>";print_r($_POST);exit();

    $operation = array(
        'name' => $_POST['name'],
        'description' => $_POST['description'],
        'rate' => $_POST['rate'],
        'attached_file' => @$attached_file,
        'active' => 1,
        'system_user_id' => @$_SESSION['logged_in_user'],
        'updated_at' => current_datetime(),
        );  
    
    if($_POST['operation_id'] != "" && $_POST['operation_id'] > 0){
        $filter = "id = '".$_POST['operation_id']."'";
        $update = $dbConnect->update_data('operations',$operation,$filter);
        if($update)
        { 
            if(isset($_POST['spares'])){
                addOperationSpares($_POST['operation_id'],$_POST['spares']);
            }
            
            $response = array("status"=>true,"message"=>"Operation updated successfully.","data"=>null);  
        }else{
            $response = array("status"=>false,"message"=>"Failed to update operation.","data"=>null);
        }
    }else{
        $operation['created_at'] = current_datetime();
        $operation_id = $dbConnect->insert_data('operations',$operation);
        if($operation_id)
        {
            if(isset($_POST['spares'])){
                addOperationSpares($operation_id,$_POST['spares']);
            }
            $response = array("status"=>true,"message"=>"New operation added successfully.","data"=>null);
        
        }else{
            $response = array("status"=>false,"message"=>"Failed to add operation.","data"=>null);
        }
    }

    echo json_encode($response);
});

$app->get('/list-operation',function(){
    global $ro_next_status_link;
    $page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
    $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
    $search = isset($_GET['search']) ? $_GET['search'] : '';
    $customer_id = isset($_GET['key']) ? $_GET['key'] : '';

    $offset = (--$page) * $limit;

    $dbConnect = new dbConnect();
    $strSQL = "SELECT id,name,description,FORMAT(rate,2) as rate
             FROM operations";
    $strSQL .=" WHERE 1";
    if(trim($search) != ''){
        $strSQL .=" AND name LIKE '%".$search."%'";
    }
    $count = $dbConnect->get_count($strSQL);
    $strSQL .= " ORDER BY id DESC";
    $strSQL_limit = $strSQL." LIMIT ".$limit." OFFSET ".$offset;
   
    $data = $dbConnect->rows_assoc($strSQL_limit);

    if($data){
        
        echo json_encode(array("status" => true,"count"=>$count,"data"=>$data,"message"=>"Operations rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No Operations"));
    }
});

$app->get('/operation/{id}', function ($request, $response, $args) {
    $dbConnect = new dbConnect();
    $strSQL = "SELECT *
                FROM operations
                WHERE id = '".$args['id']."'";
    $data = $dbConnect->row_assoc($strSQL);

    if($data){
        $strSQL = "SELECT spare_id
                FROM operation_spares
                WHERE operation_id = '".$args['id']."'";
        $spares = $dbConnect->rows_assoc($strSQL);
        $data['spares'] = array();
        if($spares){
            foreach($spares as $row)
            {
                $data['spares'][] = $row['spare_id'];
            }
        }

        echo json_encode(array("status" => true,"data"=>$data,"message"=>"Operation rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No Operation data"));
    }
});

//-------------------------------------------------------------------------------------

$app->post('/manage-service',function($request,$response,$args){
    $dbConnect = new dbConnect();

    $service = array(
        'name' => $_POST['name'],
        'description' => $_POST['description'],
        'rate' => $_POST['rate'],
        'active' => 1,
        'system_user_id' => @$_SESSION['logged_in_user'],
        'updated_at' => current_datetime(),
        );  
    
    

    if($_POST['service_id'] != "" && $_POST['service_id'] > 0){
        $filter = "id = '".$_POST['service_id']."'";
        $update = $dbConnect->update_data('services',$service,$filter);
        if($update)
        { 
            if(isset($_POST['operations'])){
                addServiceOperations($_POST['service_id'],$_POST['operations']);
            }
            $response = array("status"=>true,"message"=>"Service updated successfully.","data"=>null);
        
        }else{
            $response = array("status"=>false,"message"=>"Failed to update service.","data"=>null);
        }
    }else{
        $service['created_at'] = current_datetime();
        $service_id = $dbConnect->insert_data('services',$service);
        if($service_id)
        {
            if(isset($_POST['operations'])){
                addServiceOperations($service_id,$_POST['operations']);
            }
            $response = array("status"=>true,"message"=>"New service added successfully.","data"=>null);
        
        }else{
            $response = array("status"=>false,"message"=>"Failed to add service.","data"=>null);
        }
    }

    echo json_encode($response);
});

$app->get('/list-service',function(){
    
    $page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
    $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
    $search = isset($_GET['search']) ? $_GET['search'] : '';
    $customer_id = isset($_GET['key']) ? $_GET['key'] : '';

    $offset = (--$page) * $limit;

    $dbConnect = new dbConnect();
    $strSQL = "SELECT id,name,description,FORMAT(rate,2) as rate
             FROM services";
    $strSQL .=" WHERE 1";
    if(trim($search) != ''){
        $strSQL .=" AND name LIKE '%".$search."%'";
    }
    $count = $dbConnect->get_count($strSQL);
    $strSQL .= " ORDER BY id DESC";
    $strSQL_limit = $strSQL." LIMIT ".$limit." OFFSET ".$offset;
   
    $data = $dbConnect->rows_assoc($strSQL_limit);

    if($data){
        
        echo json_encode(array("status" => true,"count"=>$count,"data"=>$data,"message"=>"Services rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No Services"));
    }
});

$app->get('/service/{id}', function ($request, $response, $args) {
    $dbConnect = new dbConnect();
    $strSQL = "SELECT *
                FROM services
                WHERE id = '".$args['id']."'";
    $data = $dbConnect->row_assoc($strSQL);

    if($data){
        $strSQL = "SELECT operation_id
                FROM service_operations
                WHERE service_id = '".$args['id']."'";
        $operations = $dbConnect->rows_assoc($strSQL);
        $data['operations'] = array();
        if($operations){
            foreach($operations as $row)
            {
                $data['operations'][] = $row['operation_id'];
            }
        }
        echo json_encode(array("status" => true,"data"=>$data,"message"=>"Service rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No Service data"));
    }
});

//----------------------------------------------------------------------------------------
$app->post('/manage-vehicle-model',function($request,$response,$args){
    $dbConnect = new dbConnect();

    $vehicle_model = array(
        'name' => $_POST['name'],
        'active' => 1,
        'system_user_id' => @$_SESSION['logged_in_user'],
        'updated_at' => current_datetime(),
        );  

    if($_POST['vehicle_model_id'] != "" && $_POST['vehicle_model_id'] > 0){
        $filter = "id = '".$_POST['vehicle_model_id']."'";
        $update = $dbConnect->update_data('vehicle_models',$vehicle_model,$filter);
        if($update)
        { 
            $response = array("status"=>true,"message"=>"Vehicle model updated successfully.","data"=>null);
        }else{
            $response = array("status"=>false,"message"=>"Failed to update vehicle model.","data"=>null);
        }
    }else{
        $vehicle_model['created_at'] = current_datetime();
        $vehicle_model_id = $dbConnect->insert_data('vehicle_models',$vehicle_model);
        if($vehicle_model_id)
        { 
            $response = array("status"=>true,"message"=>"New vehicle model added successfully.","data"=>null);
        
        }else{
            $response = array("status"=>false,"message"=>"Failed to add vehicle model.","data"=>null);
        }
    }

    echo json_encode($response);
});

$app->get('/vehicle-model/{id}', function ($request, $response, $args) {
    $dbConnect = new dbConnect();
    $strSQL = "SELECT *
                FROM vehicle_models
                WHERE id = '".$args['id']."'";
    $data = $dbConnect->row_assoc($strSQL);

    if($data){
        echo json_encode(array("status" => true,"data"=>$data,"message"=>"Vehicle model rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No vehicle model data"));
    }
});
//-------------------------------------------------------------------------------------

$app->post('/manage-vehicle-make',function($request,$response,$args){
    $dbConnect = new dbConnect();

    $vehicle_make = array(
        'name' => $_POST['name'],
        'active' => 1,
        'system_user_id' => @$_SESSION['logged_in_user'],
        'updated_at' => current_datetime(),
        );  

    if($_POST['vehicle_make_id'] != "" && $_POST['vehicle_make_id'] > 0){
        $filter = "id = '".$_POST['vehicle_make_id']."'";
        $update = $dbConnect->update_data('vehicle_makes',$vehicle_make,$filter);
        if($update)
        { 
            $response = array("status"=>true,"message"=>"Vehicle make updated successfully.","data"=>null);
        }else{
            $response = array("status"=>false,"message"=>"Failed to update vehicle make.","data"=>null);
        }
    }else{
        $vehicle_make['created_at'] = current_datetime();
        $vehicle_make_id = $dbConnect->insert_data('vehicle_makes',$vehicle_make);
        if($vehicle_make_id)
        { 
            $response = array("status"=>true,"message"=>"New vehicle make added successfully.","data"=>null);
        
        }else{
            $response = array("status"=>false,"message"=>"Failed to add vehicle make.","data"=>null);
        }
    }

    echo json_encode($response);
});

$app->get('/vehicle-make/{id}', function ($request, $response, $args) {
    $dbConnect = new dbConnect();
    $strSQL = "SELECT *
                FROM vehicle_makes
                WHERE id = '".$args['id']."'";
    $data = $dbConnect->row_assoc($strSQL);

    if($data){
        echo json_encode(array("status" => true,"data"=>$data,"message"=>"Vehicle make rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No vehicle make data"));
    }
});

//-------------------------------------------------------------------------------------


$app->post('/operation-spares',function($request,$response,$args){
    $dbConnect = new dbConnect();

    $_POST = json_decode(file_get_contents("php://input"), true);
    foreach($_POST['spares'] as $spare_id){
        $opr_spr = array(
            'operation_id' => $_POST['operation_id'],
            'spare_id' => $spare_id,
            'system_user_id' => @$_SESSION['logged_in_user'],
            'created_at' => current_datetime(),
            'updated_at' => current_datetime()
            );  
        $insert_id[] = $dbConnect->insert_data('operaion_spares',$opr_spr);
    }

    if(count($insert_id) > 0){
        $response = array("status"=>true,"message"=>"Spares mapped for operations.","data"=>null);
    }else{
        $response = array("status"=>false,"message"=>"Failed to add spares for operation.","data"=>null);
    }
});

//------------------------------------------------------------------------------------------

//get user data
$app->get('/exists-customer/{mobile}', function ($request, $response, $args) {
    $dbConnect = new dbConnect();
    $strSQL = "SELECT customers.*
                FROM customers 
                WHERE customers.mobile = '".$args['mobile']."'";
               // echo $strSQL;exit();
    $data = $dbConnect->row_assoc($strSQL);

    if($data){
        echo json_encode(array("status" => true,"data"=>$data,"message"=>"Customer rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No Customer data"));
    }
});

$app->post('/booking','manageBooking');

$app->get('/bookings',function(){
    $page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
    $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
    $search = isset($_GET['search']) ? $_GET['search'] : '';
    $customer_id = isset($_GET['key']) ? $_GET['key'] : '';

    $offset = (--$page) * $limit;

    $dbConnect = new dbConnect();
    $strSQL = "SELECT B.*,CONCAT(C.first_name,' ',C.last_name) AS CustomerName,
                V.reg_no AS VehicleRegNo,C.mobile AS CustomerMobile
             FROM booking B
             LEFT JOIN customers C ON C.id = B.customer_id
             LEFT JOIN vehicles V ON V.id = B.vehicle_id
             WHERE B.active!=0";
    if(trim($customer_id) != ''){
        $strSQL .=" AND B.customer_id = ".$customer_id;
    }if(trim($search) != ''){
        $strSQL .=" AND (C.first_name LIKE '%".$search."%'";
        $strSQL .=" OR C.last_name LIKE '%".$search."%'";
        $strSQL .=" OR C.mobile LIKE '%".$search."%'";
        $strSQL .=" OR C.phone LIKE '%".$search."%'";
        $strSQL .=" OR C.email LIKE '%".$search."%'";
        $strSQL .=" OR V.reg_no LIKE '%".$search."%'";
        $strSQL .=" )";
    }
    $count = $dbConnect->get_count($strSQL);
    $strSQL .= " ORDER BY B.id DESC";
    $strSQL_limit = $strSQL." LIMIT ".$limit." OFFSET ".$offset;
   
    $data = $dbConnect->rows_assoc($strSQL_limit);
    if($data){
        echo json_encode(array("status" => true,"count"=>$count,"data"=>$data,"message"=>"Bookings rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No Bookings data"));
    }
});

$app->get('/todays-booking',function(){
  
    $dbConnect = new dbConnect();
    $strSQL = "SELECT B.*,CONCAT(C.first_name,' ',C.last_name) AS CustomerName,
                V.reg_no AS VehicleRegNo,C.mobile AS CustomerMobile,B.booking_date
             FROM booking B
             LEFT JOIN customers C ON C.id = B.customer_id
             LEFT JOIN vehicles V ON V.id = B.vehicle_id
             WHERE B.active!=0 AND date(B.booking_date) = '".today_sql()."'";

    $data = $dbConnect->rows_assoc($strSQL);
    if($data){
        echo json_encode(array("status" => true,"data"=>$data));
    }else{
        echo json_encode(array("status" => false,"data"=>null));
    }
});

$app->get('/booking/{id}', function ($request, $response, $args) {
    $dbConnect = new dbConnect();
    $strSQL = "SELECT B.*,C.first_name AS CustomerFirstName,C.last_name AS CustomerLastName,C.mobile AS CustomerMobile,V.reg_no as VehicleRegNo,V.vehicle_model_id,V.vehicle_make_id
                FROM booking B 
                LEFT JOIN customers C ON C.id = B.customer_id
                LEFT JOIN vehicles V ON V.id = B.vehicle_id
                WHERE B.id = '".$args['id']."'";
    $data = $dbConnect->row_assoc($strSQL);

    if($data){
        echo json_encode(array("status" => true,"data"=>$data,"message"=>"booking rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No booking data"));
    }
});

$app->post('/add-stock','addStock');
$app->post('/edit-stock','editStock');
$app->get('/view-stock',function(){

    $page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
    $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
    $search = isset($_GET['search']) ? $_GET['search'] : '';

    $offset = (--$page) * $limit;

    $dbConnect = new dbConnect();
    $strSQL = "SELECT stock.id as StockId,item.name AS ItemName,item.code AS ItemCode,stock.stock_date as StockDate,stock.mrp as MRP,stock.price as Price,stock.quantity as Quantity,item.units AS Units,stock.batch_no as Batch FROM stock_spares stock,spares item
                WHERE stock.spare_id = item.id AND stock.verified=1";
    if(trim($search) != ''){
        $strSQL .=" AND (item.name LIKE '%".$search."%'";
        $strSQL .=" OR item.code LIKE '%".$search."%'";
        $strSQL .=" OR item.description LIKE '%".$search."%'";
        $strSQL .=" )";
    }
    $count = $dbConnect->get_count($strSQL);
    $strSQL .= " ORDER BY stock.id DESC";
    $strSQL_limit = $strSQL." LIMIT ".$limit." OFFSET ".$offset;
   
    $data = $dbConnect->rows_assoc($strSQL_limit);
    if($data){
        echo json_encode(array("status" => true,"count"=>$count,"data"=>$data,"message"=>"Stock rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"count"=>$count,"data"=>null,"message"=>"No Stock data"));
    }
});

$app->get('/pending-stock',function(){

    $page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
    $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
    $search = isset($_GET['search']) ? $_GET['search'] : '';

    $offset = (--$page) * $limit;

    $dbConnect = new dbConnect();
    $strSQL = "SELECT  DATE_FORMAT(stock.stock_date,'%M %d %Y %h:%i:%s') as stock_date,stock.id,item.name as Spare,stock.mrp as MRP,stock.price as Price,stock.quantity as Quantity,stock.batch_no as Batch FROM stock_spares stock,spares item
                WHERE stock.spare_id = item.id AND verified=0 ORDER BY id DESC";


    // if(trim($search) != ''){
    //     $strSQL .=" AND (item.name LIKE '%".$search."%'";
    //     $strSQL .=" )";
    // }
    
    $count = $dbConnect->get_count($strSQL);

    $strSQL_limit = $strSQL." LIMIT ".$limit." OFFSET ".$offset;
   
    $data = $dbConnect->rows_assoc($strSQL_limit);
   
    if($data){
        echo json_encode(array("status" => true,"count"=>$count,"data"=>$data,"message"=>"Pendig Stock rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"count"=>$count,"data"=>null,"message"=>"No Unverified stock"));
    }
});

$app->post('/verify-stock',function(){
    global $app;
    
    $_POST = json_decode(file_get_contents("php://input"), true); 
    //echo json_encode($_POST) ;exit();
    $stockData = array(
        'verified' => 1,
        'updated_at' => current_datetime()
        );  

    $dbConnect = new dbConnect();
    $filter = "id = '".$_POST['id']."'";
 
    $update = $dbConnect->update_data('stock_spares',$stockData,$filter);
    if($update)
    {
        $response = array("status"=>true,"message"=>"Stock Verified.","data"=>null);

    }else{
        $response = array("status"=>false,"message"=>"Failed to verify stock.","data"=>null);
    }

    echo json_encode($response);
});

//repair orders
$app->post('/create-order','addRO');

$app->post('/edit-order','updateRO');

$app->get('/ro-pdf/{id}',function($request, $response, $args){
    $ro_id = $args['id'];

    $dbConnect = new dbConnect();
    $strSQL = "SELECT RO.*,CONCAT(C.first_name,' ',C.last_name) as Customer,
                C.address as CustomerAddress,C.email as CustomerEmail,C.mobile as CustomerMobile,
                V.reg_no,V.engine_no,V.chasis_no,V.color as VehicleColor,
                Vmakes.name as Vehicle_make,Vmodels.name as VehicleModel
             FROM repair_orders RO
             LEFT JOIN customers C ON C.id = RO.customer_id
             LEFT JOIN vehicles V ON V.id = RO.vehicle_id
             LEFT JOIN vehicle_makes Vmakes ON Vmakes.id = V.vehicle_make_id
             LEFT JOIN vehicle_models Vmodels ON Vmodels.id = V.vehicle_model_id
             
             WHERE RO.id='".$ro_id."'";
    
    $data = $dbConnect->row_assoc($strSQL);

    //ro services
    $strSQL1 = "SELECT S.name as Service FROM repair_order_services ROS
                LEFT JOIN services S ON S.id = ROS.service_id 
                 WHERE ro_id='".$ro_id."'";

    if($ro_services = $dbConnect->row_assoc($strSQL1))
        $data['service'] = $ro_services;

    //ro operations
    $strSQL2 = "SELECT ro_opr.*,opr.name AS OperationName 
                FROM repair_order_operations ro_opr,operations opr 
                WHERE ro_opr.operation_id=opr.id AND ro_opr.ro_id='".$ro_id."'";
    $ro_operations = $dbConnect->rows_assoc($strSQL2);
    $data['operations'] = ($ro_operations)?$ro_operations:array();

    //ro spares
    $data['spares'] = ro_spares($ro_id);
    //echo "<pre>";print_r($data);exit();
    //-------------------------------------------------


    $response = $this->response->withHeader( 'Content-type', 'application/pdf' );
    $response->write(ro_pdf($data) );
        
    return $response;

});

$app->get('/order-li',function(){
    $dbConnect = new dbConnect();
    $strSQL = "SELECT RO.id,CONCAT(RO.ro_number,'-',C.first_name,' ',C.last_name) AS name 
                FROM repair_orders RO
             LEFT JOIN customers C ON C.id = RO.customer_id 
             ORDER BY RO.id DESC";
    $data = $dbConnect->rows_assoc($strSQL);
    echo json_encode($data);
});

$app->get('/ro-history/{ro_id}',function($request, $response, $args){
    $dbConnect = new dbConnect();
    $strSQL = "SELECT history.notes,history.created_at,CONCAT(emp.first_name,' ',emp.last_name) AS EmployeeName,status.name as RoStatus 
        FROM repair_order_status_history history 
        LEFT JOIN repair_order_statuses status ON status.id = history.status_id 
        LEFT JOIN employees emp ON emp.id = history.employee_id";
    $strSQL .=" WHERE ro_id = '".$args['ro_id']."'";

    $data = $dbConnect->rows_assoc($strSQL);

    if($data){ 
        echo json_encode(array("status" => true,"data"=>$data,"message"=>"Orders rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No Order History"));
    }
});

$app->get('/customer-history/{customer_id}',function($request, $response, $args){
    $dbConnect = new dbConnect();
    $strSQL = "SELECT DATE_FORMAT(ord.ro_date,'%d %b %Y %h:%i:%s %p') as order_date,ro_status.name as roStatus,veh.reg_no, IF(bk.booking_date IS NULL,'No Bookings',CONCAT(DATE_FORMAT(bk.booking_date,'%d %b %Y %h:%i:%s %p'),' (',bk.booking_number,')')) as bk_detail
        FROM repair_orders ord
        LEFT JOIN repair_order_statuses ro_status ON ro_status.id= ord.ro_status_id
        LEFT JOIN vehicles veh ON veh.id = ord.vehicle_id
        LEFT JOIN booking bk ON bk.id = ord.booking_id";
        if($args['customer_id'] > 0){
            $strSQL .=" WHERE ord.customer_id = '".$args['customer_id']."'";
        }
       

    $data = $dbConnect->rows_assoc($strSQL);

    if($data){ 
        echo json_encode(array("status" => true,"data"=>$data,"message"=>"Orders rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No Order History"));
    }
});

 
$app->get('/orders-completed',function(){
    
    $page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
    $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
    $search = isset($_GET['search']) ? $_GET['search'] : '';
    $customer_id = isset($_GET['key']) ? $_GET['key'] : '';

    $offset = (--$page) * $limit;

    $dbConnect = new dbConnect();
    $strSQL = "SELECT RO.*,CONCAT(C.first_name,' ',C.last_name) AS CustomerName,
                V.reg_no AS VehicleRegNo,CONCAT(make.name,' ',model.name) as VehicleModel, C.mobile AS CustomerMobile,ro_feedback.comments FeedbackComment,feedback.id as RoFeedbackId,feedback.name as RoFeedbackName
             FROM repair_orders RO
             LEFT JOIN repair_order_feedbacks ro_feedback ON ro_feedback.ro_id = RO.id
             LEFT JOIN feedbacks feedback ON feedback.id = ro_feedback.feedback_id
             LEFT JOIN customers C ON C.id = RO.customer_id
             LEFT JOIN vehicles V ON V.id = RO.vehicle_id
             LEFT JOIN vehicle_models model ON V.vehicle_model_id = model.id
             LEFT JOIN vehicle_makes make ON V.vehicle_make_id = make.id";
    $strSQL .=" WHERE ro_status_id = '".RO_STATUS_COMPLETED."'";
    if(trim($customer_id) != ''){
        $strSQL .=" AND RO.customer_id = ".$customer_id;
    }else if(trim($search) != ''){
        $strSQL .=" AND (C.first_name LIKE '%".$search."%'";
        $strSQL .=" OR C.last_name LIKE '%".$search."%'";
        $strSQL .=" OR C.mobile LIKE '%".$search."%'";
        $strSQL .=" OR C.phone LIKE '%".$search."%'";
        $strSQL .=" OR C.email LIKE '%".$search."%'";
        $strSQL .=" OR V.reg_no LIKE '%".$search."%'";
        $strSQL .=" )";
    }
    $count = $dbConnect->get_count($strSQL);
    $strSQL .= " ORDER BY RO.id DESC";
    $strSQL_limit = $strSQL." LIMIT ".$limit." OFFSET ".$offset;
   
    $data = $dbConnect->rows_assoc($strSQL_limit);

    if($data){
        echo json_encode(array("status" => true,"count"=>$count,"data"=>$data,"message"=>"Orders rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No Order data"));
    }
});

$app->get('/orders',function(){
    global $ro_next_status_link;
    $page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
    $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
    $search = isset($_GET['search']) ? $_GET['search'] : '';
    $customer_id = isset($_GET['key']) ? $_GET['key'] : '';

    $offset = (--$page) * $limit;

    $dbConnect = new dbConnect();
    $strSQL = "SELECT RO.*,CONCAT(C.first_name,' ',C.last_name) AS CustomerName,
                V.reg_no AS VehicleRegNo,C.mobile AS CustomerMobile
             FROM repair_orders RO
             LEFT JOIN customers C ON C.id = RO.customer_id
             LEFT JOIN vehicles V ON V.id = RO.vehicle_id";
    $strSQL .=" WHERE RO.ro_status_id != '".RO_STATUS_COMPLETED."'";
    if(trim($customer_id) != ''){
        $strSQL .=" AND RO.customer_id = ".$customer_id;
    }else if(trim($search) != ''){
        $strSQL .=" AND (C.first_name LIKE '%".$search."%'";
        $strSQL .=" OR C.last_name LIKE '%".$search."%'";
        $strSQL .=" OR C.mobile LIKE '%".$search."%'";
        $strSQL .=" OR C.phone LIKE '%".$search."%'";
        $strSQL .=" OR C.email LIKE '%".$search."%'";
        $strSQL .=" OR V.reg_no LIKE '%".$search."%'";
        $strSQL .=" OR RO.ro_number LIKE '%".$search."%'";
        $strSQL .=" )";
    }
    $count = $dbConnect->get_count($strSQL);
    $strSQL .= " ORDER BY RO.id DESC";
    $strSQL_limit = $strSQL." LIMIT ".$limit." OFFSET ".$offset;
   
    $data = $dbConnect->rows_assoc($strSQL_limit);

    if($data){
        $data['next_status_links'] = $ro_next_status_link;
        echo json_encode(array("status" => true,"count"=>$count,"data"=>$data,"message"=>"Orders rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No Bookings data"));
    }
});

$app->get('/order/{id}', function ($request, $response, $args) {
    $dbConnect = new dbConnect();
    $strSQL = "SELECT RO.*,CONCAT(C.first_name,' ',C.last_name) as CustomerName,V.reg_no AS VehicleRegNo
             FROM repair_orders RO
             JOIN customers C ON  RO.customer_id = C.id 
             JOIN vehicles V ON  RO.vehicle_id = V.id 
             
             WHERE RO.id='".$args['id']."'";
    
    $data = $dbConnect->row_assoc($strSQL);

    //ro services
    $strSQL1 = "SELECT * FROM repair_order_services WHERE ro_id='".$args['id']."'";

    if($ro_services = $dbConnect->row_assoc($strSQL1))
        $data['service'] = $ro_services;

    //ro operations
    $strSQL2 = "SELECT ro_opr.*,opr.name AS OperationName 
                FROM repair_order_operations ro_opr,operations opr 
                WHERE ro_opr.operation_id=opr.id AND ro_opr.ro_id='".$args['id']."'";
    $ro_operations = $dbConnect->rows_assoc($strSQL2);
    $data['operations'] = ($ro_operations)?$ro_operations:array();

    //ro spares
    $data['spares'] = ro_spares($args['id']);

    if($data){
        echo json_encode(array("status" => true,"data"=>$data,"message"=>"order rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No order data"));
    }
});

$app->get('/order-spares/{ro_id}', function ($request, $response, $args) {
    $dbConnect = new dbConnect();
    $spares = ro_spares($args['ro_id']);
    echo json_encode($spares);
});


$app->post('/issue-spares','issueRoSpares');

$app->post('/ro-operation-complete',function(){
    //$_POST = json_decode(file_get_contents("php://input"), true);

    $ro_id = $_POST['ro_id'];
    $employee_id = (isset($_POST['employee_id']))?$_POST['employee_id'] :null;
    $comments = (isset($_POST['comments']))?$_POST['comments'] :"Repair Order Operation Completed";
   //echo json_encode($_POST) ;exit();
    $dbConnect = new dbConnect();
    //add ro status history
    add_ro_history($ro_id,$employee_id,RO_STATUS_OPERATION_COMPLETED,$comments);

     //update ro status
    $dbConnect->update_data('repair_orders',array("ro_status_id"=>RO_STATUS_OPERATION_COMPLETED),"id='".$ro_id."'");

});

$app->post('/ro-qa-complete',function(){

    //echo "<pre>";print_r($_FILES);exit();

    $ro_id = $_POST['ro_id'];
    $employee_id = (isset($_POST['employee_id']))?$_POST['employee_id'] :null;
    $notes = (isset($_POST['comments']))?$_POST['comments'] :"QA completed";
    $file_name = false;
    if(isset($_FILES['file'])){
       $file_name = upload_pdf_file($_FILES['file'],'uploads');
    }
    
    $dbConnect = new dbConnect();
    //add ro status history
    add_ro_history($ro_id,$employee_id,RO_STATUS_QA_COMPLETED,$notes);

    $ro_data = array("ro_status_id"=>RO_STATUS_QA_COMPLETED);
    if($file_name){
        $ro_data['qa_pdf'] = $file_name;
    }

     //update ro status
    $dbConnect->update_data('repair_orders',$ro_data,"id='".$ro_id."'");

});

$app->post('/ro-next-service-add',function(){
    //$_POST = json_decode(file_get_contents("php://input"), true);

    $ro_id = $_POST['ro_id'];
    $employee_id = (isset($_POST['employee_id']))?$_POST['employee_id'] :null;
    $notes = (isset($_POST['comment']))?$_POST['comment']:"Next Servcice added";
    //echo json_encode($_POST) ;exit();
    $dbConnect = new dbConnect();

    $strSQL = "SELECT RO.*
             FROM repair_orders RO
             
             WHERE RO.id='".$ro_id."'";
    
    $ro = $dbConnect->row_assoc($strSQL);


    $booking = array(
        'booking_number' => $_POST['booking_reference'],
        'booking_date' => $_POST['next_service_date'],
        'customer_id' => $ro['customer_id'],
        'vehicle_id' => $ro['vehicle_id'],
        'comments' => $_POST['comments'],
        'booking_type_id' => NEXT_SERVICE,
        'updated_at' => current_datetime(),
        'system_user_id' => @$_SESSION['logged_in_user'],
        'created_at' => current_datetime(),
        'updated_at' => current_datetime(),
        );  
    $booking_id = $dbConnect->insert_data('booking',$booking);
    if($booking_id)
    {
        //add ro status history
        add_ro_history($ro_id,$employee_id,RO_STATUS_NEXT_SERVICE_ADDED,$notes);
        //update ro status
        $dbConnect->update_data('repair_orders',array("ro_status_id"=>RO_STATUS_NEXT_SERVICE_ADDED),"id='".$ro_id."'");
        $response = array("status"=>true,"message"=>"Next service added successfully.","data"=>null);
    
    }else{
        $response = array("status"=>false,"message"=>"Failed to add Next service.","data"=>null);
    }

});

$app->post('/ro-pre-bill',function(){
    //change status and save prebill pdf in ro table
    //$_POST = json_decode(file_get_contents("php://input"), true);

    $ro_id = $_POST['ro_id'];
    $employee_id = (isset($_POST['employee_id']))?$_POST['employee_id'] :null;
    $notes = (isset($_POST['comments']))?$_POST['comments'] :"Prebill generated";
    //echo json_encode($_POST) ;exit();
    $dbConnect = new dbConnect();
    //add ro status history
    add_ro_history($ro_id,$employee_id,RO_STATUS_PREBILL_GENERATED,$notes);

     //update ro status
    $dbConnect->update_data('repair_orders',array("ro_status_id"=>RO_STATUS_PREBILL_GENERATED),"id='".$ro_id."'");

});

$app->post('/ro-paid',function(){
    //change status and save prebill pdf in ro table
    //$_POST = json_decode(file_get_contents("php://input"), true);

    $ro_id = $_POST['ro_id'];
    $employee_id = (isset($_POST['employee_id']))?$_POST['employee_id'] :null;
    $notes = (isset($_POST['comments']))?$_POST['comments'] :"Paid";
    //echo json_encode($_POST) ;exit();
    $dbConnect = new dbConnect();
    //add ro status history
    add_ro_history($ro_id,$employee_id,RO_STATUS_BILL_PAID,$notes);

     //update ro status
    $dbConnect->update_data('repair_orders',array("ro_status_id"=>RO_STATUS_BILL_PAID),"id='".$ro_id."'");

});

$app->post('/ro-complete',function(){
    //change status and save prebill pdf in ro table
    //$_POST = json_decode(file_get_contents("php://input"), true);

    $ro_id = $_POST['ro_id'];
    $employee_id = (isset($_POST['employee_id']))?$_POST['employee_id'] :null;
    $notes = (isset($_POST['comments']))?$_POST['comments'] :"Paid";
    //echo json_encode($_POST) ;exit();
    $dbConnect = new dbConnect();
    //add ro status history
    add_ro_history($ro_id,$employee_id,RO_STATUS_COMPLETED,$notes);

     //update ro status
    $dbConnect->update_data('repair_orders',array("ro_status_id"=>RO_STATUS_COMPLETED),"id='".$ro_id."'");

});

$app->get('/get-prebill/{id}', function ($request, $response, $args) {
    $dbConnect = new dbConnect();
    $strSQL = "SELECT RO.*
             FROM repair_orders RO
             
             WHERE RO.id='".$args['id']."'";
    
    $data = $dbConnect->row_assoc($strSQL);

    //ro services
    $strSQL1 = "SELECT * FROM repair_order_services WHERE ro_id='".$args['id']."'";

    if($ro_services = $dbConnect->row_assoc($strSQL1))
        $data['service'] = $ro_services;

    //ro operations
    $strSQL2 = "SELECT ro_opr.*,opr.name AS OperationName,(ro_opr.rate*ro_opr.quantity) as LineAmount 
                FROM repair_order_operations ro_opr,operations opr 
                WHERE ro_opr.operation_id=opr.id AND ro_opr.ro_id='".$args['id']."'";
    $ro_operations = $dbConnect->rows_assoc($strSQL2);
    $data['operations'] = ($ro_operations)?$ro_operations:array();

    //ro spares
    $strSQL3 = "SELECT spr_issued.spare_id,spr_issued.quantity,spr_issued.price,
                (spr_issued.quantity*spr_issued.price) as LineAmount,
                spr.name AS SpareName 
                FROM repair_order_spares_issue spr_issued, repair_order_spares ro_spr
                LEFT JOIN spares spr ON spr.id= ro_spr.spare_id
                WHERE ro_spr.id=spr_issued.repair_order_spare_id AND ro_spr.ro_id='".$args['id']."'";
    $ro_spares = $dbConnect->rows_assoc($strSQL3);
    $data['spares'] = ($ro_spares)?$ro_spares:array();




    if($data){
        echo json_encode(array("status" => true,"data"=>$data,"message"=>"order rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No order data"));
    }
});

//bill
$app->post('/create-bill','createBill');

$app->get('/list-bill',function(){
    global $ro_next_status_link;
    $page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
    $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
    $search = isset($_GET['search']) ? $_GET['search'] : '';
    $customer_id = isset($_GET['key']) ? $_GET['key'] : '';

    $offset = (--$page) * $limit;

    $dbConnect = new dbConnect();
    $strSQL = "SELECT bill.*,
    bill.bill_number,bill.amount,bill.discount,bill.tax,
    DATE_FORMAT(bill.bill_date,'%M %d %Y %h:%i:%s') as bill_date,
            CONCAT(C.first_name,' ',C.last_name) AS CustomerName,
                V.reg_no AS VehicleRegNo,C.mobile AS CustomerMobile
             FROM bills as bill 
             JOIN repair_orders RO ON RO.id = bill.ro_id
             LEFT JOIN customers C ON C.id = RO.customer_id
             LEFT JOIN vehicles V ON V.id = RO.vehicle_id";
    $strSQL .=" WHERE 1";
    if(trim($customer_id) != ''){
        $strSQL .=" AND RO.customer_id = ".$customer_id;
    }else if(trim($search) != ''){
        $strSQL .=" AND (C.first_name LIKE '%".$search."%'";
        $strSQL .=" OR C.last_name LIKE '%".$search."%'";
        $strSQL .=" OR C.mobile LIKE '%".$search."%'";
        $strSQL .=" OR C.phone LIKE '%".$search."%'";
        $strSQL .=" OR C.email LIKE '%".$search."%'";
        $strSQL .=" OR V.reg_no LIKE '%".$search."%'";
        $strSQL .=" )";
    }
    $count = $dbConnect->get_count($strSQL);
    $strSQL .= " ORDER BY bill.id DESC";
    $strSQL_limit = $strSQL." LIMIT ".$limit." OFFSET ".$offset;
   
    $data = $dbConnect->rows_assoc($strSQL_limit);

    if($data){
        
        echo json_encode(array("status" => true,"count"=>$count,"data"=>$data,"next_status_links"=>$ro_next_status_link,"message"=>"Orders rendered successfully"));
    }else{
        echo json_encode(array("status" => false,"data"=>null,"message"=>"No Bookings data"));
    }
});

$app->get('/bill-pdf/{id}',function($request, $response, $args){
    $id = $args['id'];

    $dbConnect = new dbConnect();
    $strSQL = "SELECT bill.*,CONCAT(C.first_name,' ',C.last_name) as Customer,
                C.address as CustomerAddress,C.email as CustomerEmail,C.mobile as CustomerMobile,
                V.reg_no,V.engine_no,V.chasis_no,V.color as VehicleColor,
                Vmakes.name as Vehicle_make,Vmodels.name as VehicleModel
             FROM bills bill
             JOIN repair_orders RO ON RO.id=bill.ro_id
             LEFT JOIN customers C ON C.id = RO.customer_id
             LEFT JOIN vehicles V ON V.id = RO.vehicle_id
             LEFT JOIN vehicle_makes Vmakes ON Vmakes.id = V.vehicle_make_id
             LEFT JOIN vehicle_models Vmodels ON Vmodels.id = V.vehicle_model_id
             
             WHERE bill.id='".$id."'";
    
    $data = $dbConnect->row_assoc($strSQL);


    //operations
    $strSQL2 = "SELECT bill_opr.*,opr.name AS OperationName 
                FROM bill_operations bill_opr,operations opr 
                WHERE bill_opr.operation_id=opr.id AND bill_opr.bill_id='".$id."'";
    $bill_operations = $dbConnect->rows_assoc($strSQL2);
    $data['operations'] = ($bill_operations)?$bill_operations:array();

    //spares
    $strSQL2 = "SELECT bill_spr.*,spr.name AS SpareName 
                FROM bill_spares bill_spr,spares spr 
                WHERE bill_spr.spare_id=spr.id AND bill_spr.bill_id='".$id."'";
    $bill_spares = $dbConnect->rows_assoc($strSQL2);
    $data['spares'] = ($bill_spares)?$bill_spares:array();
    //echo "<pre>";print_r($data);exit();
    //-------------------------------------------------


    $response = $this->response->withHeader( 'Content-type', 'application/pdf' );
    $response->write(bill_pdf($data) );
        
    return $response;

});
//---------------------------------

$app->post('/order-feedback','orderFeedback');



$app->run();


//functions
function check_access() 
{
    //global $session;
    
    $cookie_user = $_REQUEST['user'];

    // Get session variable:
    $dbConnect = new dbConnect();
    $strSQL = "SELECT *
                FROM autocare_sessions
                WHERE ip_address = '".$_SERVER['REMOTE_ADDR']."'";
    $sess_data = $dbConnect->row_assoc($strSQL);
    if($sess_data){
        session_id($sess_data['id']);
        $session = new \RKA\Session();
        $session_user = unserialize($sess_data['data']);
        if($session_user['id'] == $cookie_user){
            echo json_encode(array("data" => $session_user,"status" => true));
        }else{
            echo json_encode(array("data" => null,"status" => false));
        }
    }else{
        echo json_encode(array("data" => null,"status" => false));
    }

    
}

function addUser(){
    global $app;
    
    $_POST = json_decode(file_get_contents("php://input"), true); 
    //echo json_encode($_POST) ;exit();
    $empData = array(
        'first_name' => $_POST['first_name'],
        'last_name' => $_POST['last_name'],
        'phone' => $_POST['phone'],
        'email' => $_POST['email'],
        'mobile' => $_POST['mobile'],
        'address' => $_POST['address'],
        'employee_status_id' => $_POST['employee_status'],
        'employee_current_designation_id' => $_POST['designation'],
        'system_access' => $_POST['system_access'],
        'gender_id' => $_POST['gender'],
        'dob' => date2sql($_POST['dob']),
        'created_at' => current_datetime(),
        'updated_at' => current_datetime(),
        'system_user_id' => @$_SESSION['logged_in_user']
        );  

    $dbConnect = new dbConnect();
    $employee_id = $dbConnect->insert_data('employees',$empData);
    if($_POST['system_access'] == SYSTEM_ACCESS_YES){

        $userData = array(
            'user_type_id'=>$_POST['user_type'],
            'username' => $_POST['username'],
            'password' => md5($_POST['password']),
            'employee_id' => $employee_id,
            'phone' => $_POST['phone'],
            'email' => $_POST['email'],
            'created_at' => current_datetime(),
            'updated_at' => current_datetime(),
            'system_user_id' => @$_SESSION['logged_in_user']
                );
        $user_id = $dbConnect->insert_data('users',$userData);
    }
    
    if($employee_id)
    {
        $response = array("status"=>true,"message"=>"Employee added successfully.","data"=>null);

    }else{
        $response = array("status"=>false,"message"=>"Failed to add Employee.","data"=>null);
    }

    echo json_encode($response);
}

function editUser(){
    global $app;
    
    $_POST = json_decode(file_get_contents("php://input"), true); 
    //echo json_encode($_POST) ;exit();
    $empData = array(
        'first_name' => $_POST['first_name'],
        'last_name' => $_POST['last_name'],
        'phone' => $_POST['phone'],
        'email' => $_POST['email'],
        'mobile' => $_POST['mobile'],
        'address' => $_POST['address'],
        'employee_status_id' => $_POST['employee_status'],
        'employee_current_designation_id' => $_POST['designation'],
        'gender_id' => $_POST['gender'],
        'dob' => date2sql($_POST['dob']),
        'updated_at' => current_datetime(),
        'system_user_id' => @$_SESSION['logged_in_user']
        );  

    $dbConnect = new dbConnect();
    $filter = "id = '".$_POST['id']."'";
   // echo json_encode($empData) ;exit();
    $update = $dbConnect->update_data('employees',$empData,$filter);

    if($update)
    {
        $userData = array(
            'phone' => $_POST['phone'],
            'email' => $_POST['email'],
            'updated_at' => current_datetime(),
            'system_user_id' => @$_SESSION['logged_in_user']
            );
        $filterUser = "employee_id = '".$_POST['id']."'";
        $userUpdate = $dbConnect->update_data('users',$userData,$filterUser);
        $response = array("status"=>true,"message"=>"Employee updated successfully.","data"=>null);

    }else{
        $response = array("status"=>false,"message"=>"Failed to update Employee.","data"=>null);
    }

    echo json_encode($response);
}

function changePassword()
{
    global $app,$session;
    $user = $session->loggedinUser;
    $_POST = json_decode(file_get_contents("php://input"), true); 
    $response = array("status"=>true,"message"=>"Password changed successfully.","data"=>$user);

    $current_password = md5($_POST['current_password']);
    $new_password = $_POST['new_password'];
    $data = array('password' => md5($_POST['new_password']));
    $filter = "username = '".$user['username']."' AND password = '".$current_password."'";
    $dbConnect = new dbConnect();
    $update = $dbConnect->update_data('users',$data,$filter);
    if($update){
        $response = array("status"=>true,"message"=>"Password changed successfully.","data"=>null);
    }else{
       $response = array("status"=>false,"message"=>"Failed to change password.","data"=>null); 
    }
    echo json_encode($response);
}

function manageCustomer(){
    global $app;
    $_POST = json_decode(file_get_contents("php://input"), true); 
    //print_r($_POST) ;exit();
    $data = array(
        'first_name' => $_POST['first_name'],
        'last_name' => $_POST['last_name'],
        'phone' => $_POST['phone'],
        'email' => $_POST['email'],
        'mobile' => $_POST['mobile'],
        'address' => $_POST['address'],
        'gender_id' => $_POST['gender'],
        //'comments' => $_POST['comments'],
        'updated_at' => current_datetime(),
        'system_user_id' => @$_SESSION['logged_in_user']
        );  

    $dbConnect = new dbConnect();
    if(isset($_POST['id'])){
        $update = $dbConnect->update_data('customers',$data,"id='".$_POST['id']."'");
        if($update)
        {
            $response = array("status"=>true,"message"=>"Customer updated successfully.","data"=>null);

        }else{
            $response = array("status"=>false,"message"=>"Failed to update Customer.","data"=>null);
        }
    }else{
        $data['created_at'] = current_datetime();
        $customer_id = $dbConnect->insert_data('customers',$data);
        if($customer_id)
        {
            $response = array("status"=>true,"message"=>"Customer added successfully.","data"=>null);

        }else{
            $response = array("status"=>false,"message"=>"Failed to add Customer.","data"=>null);
        }
    }
    echo json_encode($response);
}


function manageBooking(){
    global $app;
    $_POST = json_decode(file_get_contents("php://input"), true); 
    
    $dbConnect = new dbConnect();
    $data = array(
        'booking_date' => $_POST['booking_date'],
        'comments' => $_POST['comments'],
        'booking_type_id' => $_POST['booking_type'],//BOOKING
        'updated_at' => current_datetime(),
        'system_user_id' => @$_SESSION['logged_in_user']
        ); 

    //insert customer 
     if($_POST['customer'] != "" && is_numeric($_POST['customer'])){
       $data['customer_id'] = $_POST['customer']; 
    }else{
        $customer_data = array('first_name'=>$_POST['firstname'],'last_name'=>$_POST['lastname'],'mobile'=>$_POST['mobile']);
        $data['customer_id'] = $dbConnect->insert_data('customers',$customer_data);
    }
    //insert vehicle 
    if(is_numeric($_POST['vehicle'])){
       $data['vehicle_id'] = $_POST['vehicle']; 
    }else{
        $vehicle_data = array('reg_no'=>$_POST['vehicle'],'customer_id'=>$data['customer_id'],
            'vehicle_make_id' => $_POST['vehicle_make'],'vehicle_model_id' => $_POST['vehicle_model']);
        $data['vehicle_id'] = $dbConnect->insert_data('vehicles',$vehicle_data);
    }

    
    if(isset($_POST['id'])){
        $update = $dbConnect->update_data('booking',$data,"id='".$_POST['id']."'");
        if($update)
        {
            $response = array("status"=>true,"message"=>"Booking updated successfully.","data"=>null);

        }else{
            $response = array("status"=>false,"message"=>"Failed to update Booking.","data"=>null);
        }
    }else{
        $data['created_at'] = current_datetime();
        $data['booking_number'] = BOOKING_NUMBER_PREFIX.get_next_booking_reference();

        //echo "<pre>";print_r($data);exit();

        $booking_id = $dbConnect->insert_data('booking',$data);
        if($booking_id)
        {
            $response = array("status"=>true,"message"=>"Booking added successfully.","data"=>null);

        }else{
            $response = array("status"=>false,"message"=>"Failed to add Booking.","data"=>null);
        }
    }
    echo json_encode($response);
}

function get_next_booking_reference()
{
    $dbConnect = new dbConnect();
    // $strSQL = "SELECT  LPAD(SUBSTRING(MAX(booking_number),3,LENGTH(booking_number))+1,IF(LENGTH(MAX(booking_number)) > 5,LENGTH(MAX(booking_number))-2,3),0) as next_booking_number
    //     FROM booking ";

     $strSQL = "SELECT  IFNULL(MAX(id)+1,1) as next_booking_number
        FROM booking ";
    $row = $dbConnect->row_assoc($strSQL);
    return $row['next_booking_number'];
}

function get_next_ro_reference()
{
    $dbConnect = new dbConnect();
    // $strSQL = "SELECT  LPAD(SUBSTRING(MAX(booking_number),3,LENGTH(booking_number))+1,IF(LENGTH(MAX(booking_number)) > 5,LENGTH(MAX(booking_number))-2,3),0) as next_booking_number
    //     FROM booking ";

     $strSQL = "SELECT  IFNULL(MAX(id)+1,1) as next_ro_number
        FROM repair_orders ";
    $row = $dbConnect->row_assoc($strSQL);
    return $row['next_ro_number'];
}

function get_next_bill_reference()
{
    $dbConnect = new dbConnect();
    // $strSQL = "SELECT  LPAD(SUBSTRING(MAX(booking_number),3,LENGTH(booking_number))+1,IF(LENGTH(MAX(booking_number)) > 5,LENGTH(MAX(booking_number))-2,3),0) as next_booking_number
    //     FROM booking ";

     $strSQL = "SELECT  IFNULL(MAX(id)+1,1) as next_bill_number
        FROM bills ";
    $row = $dbConnect->row_assoc($strSQL);
    return $row['next_bill_number'];
}

function addStock(){
    global $app;
    $_POST = json_decode(file_get_contents("php://input"), true); 
    //print_r($_POST) ;exit();
    $dbConnect = new dbConnect();
    $spare = $_POST['spare'];
    $mrp = $_POST['mrp'];
    $price = $_POST['price'];
    $quantity = $_POST['quantity'];
    $batch = $_POST['batch'];
    $stock_date = date("Y-m-d h:i:s",strtotime($_POST['stock_date']));
    $stock_spare_id = array();
    
    $data = array(
        'spare_id' => $spare,
        'mrp' => $mrp,
        'price' => $price,
        'quantity' => $quantity,
        'stock_date' => $stock_date,
        'batch_no' => $batch,
        'system_user_id' => @$_SESSION['logged_in_user'],
        'created_at' => current_datetime(),
        'updated_at' => current_datetime()
        );  

    $stock_spare_id = $dbConnect->insert_data('stock_spares',$data);
    
    
    if($stock_spare_id)
    {
        $response = array("status"=>true,"message"=>"Stock added successfully.","data"=>null);

    }else{
        $response = array("status"=>false,"message"=>"Failed to add stock.","data"=>null);
    }
    
    echo json_encode($response);
}

function editStock(){
    global $app;
    $_POST = json_decode(file_get_contents("php://input"), true); 
    //print_r($_POST) ;exit();
    $data = array(
        'spare_id' => $_POST['spare_id'],
        'mrp' => $_POST['mrp'],
        'price' => $_POST['price'],
        'quantity' => $_POST['quantity'],
        'stock' => $_POST['stock'],
        'batch_no' => $_POST['batch_no'],
        'system_user_id' => @$_SESSION['logged_in_user'],
        'updated_at' => current_datetime()
        );  

    $dbConnect = new dbConnect();
    $update = $dbConnect->update_data('stock_spares',$data,"id='".$_POST['stock_spare_id']."'");
    if($update)
    {
        $response = array("status"=>true,"message"=>"Stock updated successfully.","data"=>null);

    }else{
        $response = array("status"=>false,"message"=>"Failed to edit stock.","data"=>null);
    }
    
    echo json_encode($response);
}

function addRO(){

    global $app;
    $_POST = json_decode(file_get_contents("php://input"), true); 
    //echo json_encode($_POST) ;exit();
    $dbConnect = new dbConnect();
    $ro_number = RO_NUMBER_PREFIX.get_next_ro_reference();
    $ro_date = date("Y-m-d h:i:s",strtotime($_POST['ro_date']));
    $booking_id = $_POST['booking_id'];

    if(is_numeric($_POST['customer'])){
       $customer_id = $_POST['customer']; 
    }else{
        $customer_id = $dbConnect->insert_data('customers',array('first_name'=>$_POST['customer']));
    }

    if(is_numeric($_POST['vehicle'])){
       $vehicle_id = $_POST['customer']; 
    }else{
        $vehicle_id = $dbConnect->insert_data('vehicles',array('reg_no'=>$_POST['vehicle'],'customer_id'=>$customer_id,"vehicle_model_id"=>$_POST['vehicle_model'],"vehicle_make_id"=>$_POST['vehicle_make']));
    }
    
    $employee_id = $_POST['employee_id'];
    $ro_status_id = RO_STATUS_STARTED;

    $service_id = $_POST['service'];
    $operations = $_POST['operations'];
    $spares = $_POST['spares'];
    
    
    $data = array(
        'ro_number' => $ro_number,
        'ro_date' => $ro_date,
        'booking_id' => $booking_id,
        'customer_id' => $customer_id,
        'vehicle_id' => $vehicle_id,
        'employee_id' => $employee_id,
        'ro_status_id' => $ro_status_id,
        'system_user_id' => @$_SESSION['logged_in_user'],
        'created_at' => current_datetime(),
        'updated_at' => current_datetime()
        );  

    $ro_id = $dbConnect->insert_data('repair_orders',$data);
    
    if($ro_id)
    {
        //add ro status history
        add_ro_history($ro_id,$employee_id,RO_STATUS_STARTED);

        //add ro services
        if(count($service_id) > 0){
            $iservices = addRoServices($ro_id,array($service_id));
        }

        //add ro operations
        if(count($operations) > 0){
            $ioperations = addRoOperations($ro_id,$operations);
        }

        //add ro spares
        if(count($spares) > 0){
            $ispares = addRoSpares($ro_id,$spares);
        }
        $response = array("status"=>true,"message"=>"Order created successfully.","data"=>null);

    }else{
        $response = array("status"=>false,"message"=>"Failed to create order.","data"=>null);
    }
    
    echo json_encode($response);
}

function updateRO(){
    global $app;
    $_POST = json_decode(file_get_contents("php://input"), true); 
    //echo json_encode($_POST) ;exit();
    $dbConnect = new dbConnect();
    $ro_id = $_POST['ro_id'];
    //$ro_number = $_POST['ro_number'];
    $ro_date = date("Y-m-d h:i:s",strtotime($_POST['ro_date']));
    $booking_id = $_POST['booking_id'];

    if(is_numeric($_POST['customer'])){
       $customer_id = $_POST['customer']; 
    }else{
        $customer_id = $dbConnect->insert_data('customers',array('first_name'=>$_POST['customer']));
    }

    if(is_numeric($_POST['vehicle'])){
       $vehicle_id = $_POST['customer']; 
    }else{
        $vehicle_id = $dbConnect->insert_data('vehicles',array('reg_no'=>$_POST['vehicle'],'customer_id'=>$customer_id));
    }
    
    $employee_id = $_POST['employee_id'];

    $service_id = $_POST['service'];
    $operations = $_POST['operations'];
    $spares = $_POST['spares'];
    
    $data = array(
        //'ro_number' => $ro_number,
        'ro_date' => $ro_date,
        'booking_id' => $booking_id,
        'customer_id' => $customer_id,
        'vehicle_id' => $vehicle_id,
        'employee_id' => $employee_id,
        'system_user_id' => @$_SESSION['logged_in_user'],
        'created_at' => current_datetime(),
        'updated_at' => current_datetime()
        );  

    $update = $dbConnect->update_data('repair_orders',$data,"id='".$ro_id."'");
    
    if($ro_id)
    {
        

        //add ro services
        if(count($service_id) > 0){
            $dbConnect->delete_data("repair_order_services","ro_id='".$ro_id."'");
            $iservices = addRoServices($ro_id,array($service_id));
        }

        //add ro operations
        if(count($operations) > 0){
            $dbConnect->delete_data("repair_order_operations","ro_id='".$ro_id."'");
            $ioperations = addRoOperations($ro_id,$operations);
        }

        //add ro spares
        if(count($spares) > 0){
             $dbConnect->delete_data("repair_order_spares","ro_id='".$ro_id."'");
            $ispares = addRoSpares($ro_id,$spares);
        }
        $response = array("status"=>true,"message"=>"Order updated successfully.","data"=>null);

    }else{
        $response = array("status"=>false,"message"=>"Failed to update order.","data"=>null);
    }
    
    echo json_encode($response);
}

function add_ro_history($ro_id,$employee_id,$ro_status_id,$notes='')
{
    $dbConnect = new dbConnect();
    $data = array('ro_id'       => $ro_id,
                'employee_id'   => $employee_id,
                'status_id'     => $ro_status_id,
                'notes'         => $notes,
                'system_user_id'=> @$_SESSION['logged_in_user'],
                'created_at'    => current_datetime(),
                'updated_at'    => current_datetime()
        );
    $id = $dbConnect->insert_data('repair_order_status_history',$data);
    return $id;
}


function addRoServices($ro_id,$services){

    $dbConnect = new dbConnect();
    $insert_ro_services = array();
    foreach ($services as $service) {
        $data = array(
        'ro_id' => $ro_id,
        'service_id' => $service,
        'system_user_id' => @$_SESSION['logged_in_user'],
        'created_at' => current_datetime(),
        'updated_at' => current_datetime()
        );  

        $insert_ro_services[] = $dbConnect->insert_data('repair_order_services',$data);
    }
    return $insert_ro_services;   
}

function addRoOperations($ro_id,$operations){
    $dbConnect = new dbConnect();
    $insert_ro_operations = array();
    foreach ($operations as $operation) {
        $data = array(
        'ro_id' => $ro_id,
        'operation_id' => $operation['id'],
        'rate' => $operation['rate'],
        'quantity' => $operation['quantity'],
        'system_user_id' => @$_SESSION['logged_in_user'],
        'created_at' => current_datetime(),
        'updated_at' => current_datetime()
        );  

        $insert_ro_operations[] = $dbConnect->insert_data('repair_order_operations',$data);
    }
    return $insert_ro_operations;  
}

function addRoSpares($ro_id,$spares){
    $dbConnect = new dbConnect();
    $insert_ro_spares = array();
    foreach ($spares as $spare) {
        $data = array(
        'ro_id' => $ro_id,
        'spare_id' => $spare['id'],
        'quantity' => $spare['quantity'],
        'system_user_id' => @$_SESSION['logged_in_user'],
        'created_at' => current_datetime(),
        'updated_at' => current_datetime()
        );  

        $insert_ro_spares[] = $dbConnect->insert_data('repair_order_spares',$data);
    }
    return $insert_ro_spares;  
}

function issueRoSpares()
{
    global $app;
    $_POST = json_decode(file_get_contents("php://input"), true); 
    //echo json_encode($_POST) ;exit();
    $dbConnect = new dbConnect();
    $ro_spares = $_POST['ro_spares'];
    $spares = $_POST['spares'];
    $mrp = $_POST['mrp'];
    $price = $_POST['price'];
    $quantity = $_POST['quantity'];
    $issue_date = $_POST['issue_date'];
    $ro_id = $_POST['ro_id'];
    $issued_by = $_POST['issued_by'];
    $employee_id = $_POST['mechanic'];
    
    foreach ($ro_spares as $i=>$ro_spare) {
        if($quantity[$i] > 0){
            $data = array(
            'repair_order_spare_id' => $ro_spare,
            'spare_id' => $spares[$i],
            'quantity' => $quantity[$i],
            'mrp' => $mrp[$i],
            'price' => $price[$i],
            'issued_date' => date('Y-m-d H:i:s',strtotime($issue_date)),
            'issued_user_id' => $issued_by,
            'employee_id' => $employee_id,
            'system_user_id' => @$_SESSION['logged_in_user'],
            'created_at' => current_datetime(),
            'updated_at' => current_datetime()
            );  
            $insert_ro_spare = $dbConnect->insert_data('repair_order_spares_issue',$data);
            if($insert_ro_spare){  
                //negative stock adjustment
                $stockData = array(
                    'spare_id' => $spares[$i],
                    'quantity' => -$quantity[$i],
                    'stock_date' => current_datetime(),
                    'system_user_id' => @$_SESSION['logged_in_user'],
                    'created_at' => current_datetime(),
                    'updated_at' => current_datetime()
                    ); 
                $dbConnect->insert_data('stock_spares',$stockData);
            }
        }
    }

    //update ro status
    $update = $dbConnect->update_data('repair_orders',array("ro_status_id"=>RO_STATUS_SPARE_ISSUED),"id='".$ro_id."'");
    //add ro status history
    add_ro_history($ro_id,$employee_id,RO_STATUS_SPARE_ISSUED,"Spares Issued");

    if($update)
    {
        $response = array("status"=>true,"message"=>"Spares issued successfully.","data"=>null);
    }else{
        $response = array("status"=>false,"message"=>"Failed to issue spares.","data"=>null);
    }
    echo json_encode($response);
}

function addBillOperations($bill_id,$operations,$qty,$price,$date,$emp){
    $dbConnect = new dbConnect();
    $insert_bill_operations = array();
    foreach ($operations as $i=>$operation) {
        $data = array(
        'bill_id' => $bill_id,
        'operation_id' => $operation,
        'rate' => $price[$i],
        'quantity' => $qty[$i],
        'operation_date' => $date[$i],
        'employee_id' => $emp[$i],
        'system_user_id' => @$_SESSION['logged_in_user'],
        'created_at' => current_datetime(),
        'updated_at' => current_datetime()
        );  

        $insert_bill_operations[] = $dbConnect->insert_data('bill_operations',$data);
    }
    return $insert_bill_operations;  
}

function addBillSpares($bill_id,$spares,$qty,$price){
    $dbConnect = new dbConnect();
    $insert_bill_spares = array();
    foreach ($spares as $i=>$spare) {
        $data = array(
        'bill_id' => $bill_id,
        'spare_id' => $spare,
        'quantity' => $qty[$i],
        'price' => $price[$i],
        'system_user_id' => @$_SESSION['logged_in_user'],
        'created_at' => current_datetime(),
        'updated_at' => current_datetime()
        );  

        $insert_bill_spares[] = $dbConnect->insert_data('bill_spares',$data);
    }
    return $insert_bill_spares;  
}

//-------------------------------------------------------------------------------------
function manageRoServices($ro_id,$services)
{
    $dbConnect = new dbConnect();
    foreach ($services as $service) {
        $ro_service_id = $service['ro_service_id'];
        if($ro_service_id == -1){
            $data = array(
            'ro_id' => $ro_id,
            'service_id' => $service,
            'system_user_id' => @$_SESSION['logged_in_user'],
            'created_at' => current_datetime(),
            'updated_at' => current_datetime()
            );  

            $dbConnect->insert_data('repair_order_services',$data);
        }else{
            $data = array(
                'ro_id' => $ro_id,
                'service_id' => $service,
                'updated_at' => current_datetime()
                ); 
            $dbConnect->update_data('repair_order_services',$data,"id='".$ro_service_id."'");  
        } 
    }//for loop
}

function manageRoOperations($ro_id,$operations){
    $dbConnect = new dbConnect();
    foreach ($operations as $operation) {
        $ro_operation_id = $operation['ro_operation_id'];
        if($ro_operation_id == -1){
            $data = array(
            'ro_id' => $ro_id,
            'operation_id' => $operation['id'],
            'rate' => $operation['rate'],
            'quantity' => $operation['quantity'],
            'system_user_id' => @$_SESSION['logged_in_user'],
            'created_at' => current_datetime(),
            'updated_at' => current_datetime()
            );  

            $dbConnect->insert_data('repair_order_operations',$data);
        }else{
            $data = array(
            'ro_id' => $ro_id,
            'operation_id' => $operation['id'],
            'rate' => $operation['rate'],
            'quantity' => $operation['quantity'],
            'updated_at' => current_datetime()
            );  
            $dbConnect->update_data('repair_order_operations',$data,"id='".$ro_operation_id."'");
        }
    }
}

function manageRoSpares($ro_id,$spares){
    $dbConnect = new dbConnect();
    foreach ($spares as $spare) {
        $ro_spare_id = $spare['ro_spare_id'];
        if($ro_spare_id == -1){
            $data = array(
            'ro_id' => $ro_id,
            'spare_id' => $spare['id'],
            'quantity' => $spare['quantity'],
            'system_user_id' => @$_SESSION['logged_in_user'],
            'created_at' => current_datetime(),
            'updated_at' => current_datetime()
            );  
            $dbConnect->insert_data('repair_order_spares',$data);
        }else{
            $data = array(
            'ro_id' => $ro_id,
            'spare_id' => $spare['id'],
            'quantity' => $spare['quantity'],
            'updated_at' => current_datetime()
            );  
            $dbConnect->update_data('repair_order_spares',$data,"id='".$ro_spare_id."'");
        }  
    }
}

function ro_spares($ro_id)
{
    $dbConnect = new dbConnect();
    $strSQL = "SELECT ro_spr.*,spr.name AS SpareName,spr.units AS SpareUnit,spr.rate AS SpareRate 
                FROM repair_order_spares ro_spr,spares spr 
                WHERE ro_spr.spare_id = spr.id AND ro_spr.ro_id='".$ro_id."'";
    $ro_spares = $dbConnect->rows_assoc($strSQL);
    if($ro_spares){
        foreach ($ro_spares as $index => $ro_spare) {
            $strSQL1 = "SELECT AVG(mrp) AS MRP,AVG(price) AS Price FROM stock_spares
                        WHERE spare_id = '".$ro_spare['spare_id']."'";
            $avg = $dbConnect->row_assoc($strSQL1);
            $ro_spares[$index]['mrp'] = round($avg['MRP'],2);
            $ro_spares[$index]['price'] = round($avg['Price'],2); 
            $ro_spares[$index]['qoh'] =  get_qoh($ro_spare['spare_id']);
        }
        
    }
    return ($ro_spares)?$ro_spares:array();
}


function createBill(){
    global $app;
    $_POST = json_decode(file_get_contents("php://input"), true); 
    //echo json_encode($_POST) ;exit();
    $dbConnect = new dbConnect();
    $bill_number = BILL_NUMBER_PREFIX.get_next_bill_reference();
    $bill_date = date("Y-m-d h:i:s",strtotime($_POST['bill_date']));
    $ro_id = $_POST['ro_id'];
    $amount = $_POST['amount']; 
    $discount = $_POST['discount'];
    $tax = $_POST['tax'];

    //$service_id = $_POST['service'];
    $operations = $_POST['operations'];
    $spares = $_POST['spares'];
    
    
    $data = array(
        'bill_date' => $bill_date,
        'bill_number'=>$bill_number,
        'ro_id' => $ro_id,
        'amount' => $amount,
        'discount' => $discount,
        'tax' => $tax,
        'bill_status_id' => BILL_STATUS_PENDING,
        'system_user_id' => @$_SESSION['logged_in_user'],
        'created_at' => current_datetime(),
        'updated_at' => current_datetime()
        );  

    $bill_id = $dbConnect->insert_data('bills',$data);
    
    if($bill_id)
    {
       
        //add bill operations
        if(count($operations) > 0){
            $ioperations = addBillOperations($bill_id,$operations,$_POST['operation_quantity'],$_POST['operation_rate'],$_POST['operation_date'],$_POST['operation_employee']);
        }

        //add bill spares
        if(count($spares) > 0){
            $ispares = addBillSpares($bill_id,$spares,$_POST['spare_quantity'],$_POST['spare_price']);
        }

        add_ro_history($ro_id,$employee_id=0,RO_STATUS_BILL_GENERATED,"Bill generated");
        //update ro status
        $dbConnect->update_data('repair_orders',array("ro_status_id"=>RO_STATUS_BILL_GENERATED),"id='".$ro_id."'");

        $response = array("status"=>true,"message"=>"Bill created successfully.","data"=>null);

    }else{
        $response = array("status"=>false,"message"=>"Failed to create Bill.","data"=>null);
    }
    
    echo json_encode($response);
}

function orderFeedback()
{
    global $app;
    $_POST = json_decode(file_get_contents("php://input"), true); 
    //echo json_encode($_POST) ;exit();
    $dbConnect = new dbConnect();
    $data = array(
        'ro_id'         => $_POST['ro_id'],
        'feedback_id'   => $_POST['ro_feedback'],   
        'comments'      => $_POST['comments'],
        'system_user_id'=> @$_SESSION['logged_in_user'],
        'created_at'    => current_datetime(),
        'updated_at'    => current_datetime()
        );
    //print_r($data) ;exit();
    $ro_feedback_id = $dbConnect->insert_data('repair_order_feedbacks',$data);
    
    if($ro_feedback_id)
    { 
        $response = array("status"=>true,"message"=>"Feedback added successfully.","data"=>null);

    }else{
        $response = array("status"=>false,"message"=>"Failed to add feedback.","data"=>null);
    }
    echo json_encode($response);
}

//---------------------------------------------------------------------------
function addOperationSpares($operation_id,$spares){
    $dbConnect = new dbConnect();
    $insert_opr_spares = array();
    $dbConnect->delete_data('operation_spares',"operation_id = '".$operation_id."'");
    foreach ($spares as $spare_id) {
        $data = array(
        'operation_id' => $operation_id,
        'spare_id' => $spare_id,
        'system_user_id' => @$_SESSION['logged_in_user'],
        'created_at' => current_datetime(),
        'updated_at' => current_datetime()
        );  

        $insert_opr_spares[] = $dbConnect->insert_data('operation_spares',$data);
    }
    return $insert_opr_spares;  
}

function addServiceOperations($service_id,$operations){
    $dbConnect = new dbConnect();
    $insert_service_operations = array();
    $dbConnect->delete_data('service_operations',"service_id = '".$service_id."'");
    foreach ($operations as $operation_id) {
        $data = array(
        'service_id' => $service_id,
        'operation_id' => $operation_id,
        'system_user_id' => @$_SESSION['logged_in_user'],
        'created_at' => current_datetime(),
        'updated_at' => current_datetime()
        );  

        $insert_service_operations[] = $dbConnect->insert_data('service_operations',$data);
    }
    return $insert_service_operations;  
}
//--------------------------------------------------------------------------

function get_qoh($spare_id,$datetime= null)
{
    $dbConnect = new dbConnect();
    $strSQL = "SELECT SUM(quantity) AS QOH FROM stock_spares
                        WHERE spare_id = '".$spare_id."'";
    if($datetime != null)
    {
        $strSQL .= " AND stock_date <= '".$datetime."'";
    }
    $stock = $dbConnect->row_assoc($strSQL);
    return $stock['QOH'];
}


//MD/CEO  dashboard content
function dashboardContent1()
{
    $dbConnect = new dbConnect();
    $current_date = date2sql();
    $first_date = firstDate();

    $first_date_prev_month = firstDatePrevMonth();
    $last_date_prev_month = lastDatePrevMonth();
    //tiles
    $tiles = array();
    $strSQL = "SELECT COUNT(id) as num FROM booking WHERE booking_date BETWEEN '".$first_date."' AND '".$current_date."'";
    
    $row = $dbConnect->row_assoc($strSQL);
    $tiles['tile1'] = $row['num'];

    $strSQL = "SELECT COUNT(id) as num FROM repair_orders WHERE ro_date BETWEEN '".$first_date."' AND '".$current_date."'";
    $row = $dbConnect->row_assoc($strSQL);
    $tiles['tile2'] = $row['num'];

    $strSQL = "SELECT COUNT(id) as num FROM booking WHERE booking_date BETWEEN '".$first_date_prev_month."' AND '".$last_date_prev_month."'";
    $row = $dbConnect->row_assoc($strSQL);
    $tiles['tile3'] = $row['num'];

    $strSQL = "SELECT COUNT(id) as num FROM repair_orders WHERE ro_date BETWEEN '".$first_date_prev_month."' AND '".$last_date_prev_month."'";
    $row = $dbConnect->row_assoc($strSQL);
    $tiles['tile4'] = $row['num'];

    $data['tiles'] = $tiles;

    return $data;

}

//Manager In Charge  dashboard content
function dashboardContent2()
{
    $dbConnect = new dbConnect();
    $to = date2sql();
    $from = date('Y-m-d', strtotime('-7 days'));

    //tiles
    $tiles = array();
    $strSQL = "SELECT COUNT(id) as num FROM booking WHERE booking_date BETWEEN '".$from."' AND '".$to."'";
    
    $row = $dbConnect->row_assoc($strSQL);
    $tiles['tile1'] = $row['num'];

    $strSQL = "SELECT COUNT(id) as num FROM repair_orders WHERE ro_date BETWEEN '".$from."' AND '".$to."'";
    $row = $dbConnect->row_assoc($strSQL);
    $tiles['tile2'] = $row['num'];

    $strSQL = "SELECT COUNT(id) as num FROM booking WHERE booking_date = '".$to."'";
    $row = $dbConnect->row_assoc($strSQL);
    $tiles['tile3'] = $row['num'];

    $strSQL = "SELECT COUNT(id) as num FROM repair_orders WHERE ro_date = '".$to."'";
    $row = $dbConnect->row_assoc($strSQL);
    $tiles['tile4'] = $row['num'];

    $data['tiles'] = $tiles;

    return $data;

}

//Spare Parts Manager dashboard content
function dashboardContent3()
{
    $dbConnect = new dbConnect();
    $to = date2sql();
    $from = date('Y-m-d', strtotime('-7 days'));

    //tiles
    $tiles = array();
    $strSQL = "SELECT COUNT(id) as num FROM repair_orders 
            WHERE ro_date BETWEEN ('".$from."' AND '".$to."') AND ro_status_id >= '".RO_STATUS_SPARE_ISSUED."'";
    $row = $dbConnect->row_assoc($strSQL);
    $tiles['tile1'] = $row['num'];

    $strSQL = "SELECT COUNT(id) as num FROM repair_orders 
    WHERE ro_date BETWEEN ('".$from."' AND '".$to."') AND ro_status_id = '".RO_STATUS_COMPLETED."'";
    $row = $dbConnect->row_assoc($strSQL);
    $tiles['tile2'] = $row['num'];

    $strSQL = "SELECT COUNT(id) as num FROM repair_orders 
    WHERE ro_date BETWEEN ('".$from."' AND '".$to."') AND ro_status_id = '".RO_STATUS_SPARE_ISSUED."'";
    $row = $dbConnect->row_assoc($strSQL);
    $tiles['tile3'] = $row['num'];

     $strSQL = "SELECT COUNT(id) as num FROM repair_orders 
    WHERE ro_date BETWEEN ('".$from."' AND '".$to."') AND ro_status_id >= '".RO_STATUS_SPARE_ISSUED."' AND ro_status_id != '".RO_STATUS_COMPLETED."'";
    $row = $dbConnect->row_assoc($strSQL);
    $tiles['tile4'] = $row['num'];

    $data['tiles'] = $tiles;

    return $data;

}

//Customer Care Manager dashboard content
function dashboardContent5()
{
    $dbConnect = new dbConnect();
    $to = date2sql();
    $from = date('Y-m-d', strtotime('-7 days'));

    //tiles
    $tiles = array();
    $strSQL = "SELECT COUNT(id) as num FROM booking WHERE booking_date BETWEEN '".$from."' AND '".$to."'";
    
    $row = $dbConnect->row_assoc($strSQL);
    $tiles['tile1'] = $row['num'];

    $strSQL = "SELECT COUNT(id) as num FROM repair_orders WHERE ro_date BETWEEN '".$from."' AND '".$to."'";
    $row = $dbConnect->row_assoc($strSQL);
    $tiles['tile2'] = $row['num'];

    $strSQL = "SELECT COUNT(id) as num FROM booking WHERE booking_date = '".$to."'";
    $row = $dbConnect->row_assoc($strSQL);
    $tiles['tile3'] = $row['num'];

    $strSQL = "SELECT COUNT(id) as num FROM repair_orders WHERE ro_date = '".$to."'";
    $row = $dbConnect->row_assoc($strSQL);
    $tiles['tile4'] = $row['num'];

    $data['tiles'] = $tiles;

    return $data;

}

//-------------------------------------------------------------------
function upload_pdf_file($file,$dir)
{
    
    if ($file['size'] == 0) {
        return false;
    }
    $file_name = sha1(rand().microtime()).$file['name'];
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $mime = finfo_file($finfo, $file['tmp_name']);
    //echo $mime;exit();
    $ok = false;
    if ($mime == 'application/pdf') {
        $target_file = $dir."/".$file_name;
        if(!file_exists($target_file) && move_uploaded_file($file["tmp_name"], $target_file)){
            return $file_name;
        }
        else {
            return false;
        }           
    }else{
        return false;
    }
    

}



//-------------------------------------------------------------------------------------
function date2sql($datetime= null)
{
    date_default_timezone_set('Asia/Kolkata');
    if(is_null($datetime) || $datetime=='')
        return date('Y-m-d');
    else
        return date('Y-m-d',strtotime($datetime));
}
function firstDate()
{
    date_default_timezone_set('Asia/Kolkata');
    return date('Y-m-01');
    
}

function firstDatePrevMonth()
{
    date_default_timezone_set('Asia/Kolkata');
    return date('Y-m-d', strtotime('first day of previous month'));
    
}
function lastDatePrevMonth()
{
    date_default_timezone_set('Asia/Kolkata');
    return date('Y-m-d', strtotime('last day of previous month'));
    
}

function current_datetime()
{
    date_default_timezone_set('Asia/Kolkata');
    return date('Y-m-d H:i:s');
}

function add_minutes($min)
{
    date_default_timezone_set('Asia/Kolkata');
    $new_time = date('Y-m-d H:i:s', strtotime('+'.$min.' minutes'));
    return $new_time;
}


function today_sql()
{
    date_default_timezone_set('Asia/Kolkata');
    return date('Y-m-d');
}